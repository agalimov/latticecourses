<!DOCTYPE html>
<html lang="en">

<head>
	<title>Learning Lattices</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="/storage/images/favicon.ico">
	
	<!-- JQuery -->
	<script src="/js/jquery/jquery-3.1.0.min.js"></script>
	
	<!-- LESS -->
	<link rel="stylesheet/less" type="text/css" href="/css/styles.less" />	
	<script src="/js/less/less.js" type="text/javascript"></script>	
	
	<!-- Bootstrap -->
	<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">	
	<script src="/bootstrap/js/bootstrap.min.js"></script>
		
	<!-- Sigma library -->
	<script src="/js/sigmajs/src/sigma.core.js"></script>
	<script src="/js/sigmajs/src/conrad.js"></script>
	<script src="/js/sigmajs/src/utils/sigma.utils.js"></script>
	<script src="/js/sigmajs/src/utils/sigma.polyfills.js"></script>
	<script src="/js/sigmajs/src/sigma.settings.js"></script>
	<script src="/js/sigmajs/src/classes/sigma.classes.dispatcher.js"></script>
	<script src="/js/sigmajs/src/classes/sigma.classes.configurable.js"></script>
	<script src="/js/sigmajs/src/classes/sigma.classes.graph.js"></script>
	<script src="/js/sigmajs/src/classes/sigma.classes.camera.js"></script>
	<script src="/js/sigmajs/src/classes/sigma.classes.quad.js"></script>
	<script src="/js/sigmajs/src/classes/sigma.classes.edgequad.js"></script>
	<script src="/js/sigmajs/src/captors/sigma.captors.mouse.js"></script>
	<script src="/js/sigmajs/src/captors/sigma.captors.touch.js"></script>
	<script src="/js/sigmajs/src/renderers/sigma.renderers.canvas.js"></script>
	<script src="/js/sigmajs/src/renderers/sigma.renderers.webgl.js"></script>
	<script src="/js/sigmajs/src/renderers/sigma.renderers.svg.js"></script>
	<script src="/js/sigmajs/src/renderers/sigma.renderers.def.js"></script>
	<script src="/js/sigmajs/src/renderers/webgl/sigma.webgl.nodes.def.js"></script>
	<script src="/js/sigmajs/src/renderers/webgl/sigma.webgl.nodes.fast.js"></script>
	<script src="/js/sigmajs/src/renderers/webgl/sigma.webgl.edges.def.js"></script>
	<script src="/js/sigmajs/src/renderers/webgl/sigma.webgl.edges.fast.js"></script>
	<script src="/js/sigmajs/src/renderers/webgl/sigma.webgl.edges.arrow.js"></script>
	<script src="/js/sigmajs/src/renderers/canvas/sigma.canvas.labels.def.js"></script>
	<script src="/js/sigmajs/src/renderers/canvas/sigma.canvas.hovers.def.js"></script>
	<script src="/js/sigmajs/src/renderers/canvas/sigma.canvas.nodes.def.js"></script>
	<script src="/js/sigmajs/src/renderers/canvas/sigma.canvas.edges.def.js"></script>
	<script src="/js/sigmajs/src/renderers/canvas/sigma.canvas.edges.curve.js"></script>
	<script src="/js/sigmajs/src/renderers/canvas/sigma.canvas.edges.arrow.js"></script>
	<script src="/js/sigmajs/src/renderers/canvas/sigma.canvas.edges.curvedArrow.js"></script>
	<script src="/js/sigmajs/src/renderers/canvas/sigma.canvas.edgehovers.def.js"></script>
	<script src="/js/sigmajs/src/renderers/canvas/sigma.canvas.edgehovers.curve.js"></script>
	<script src="/js/sigmajs/src/renderers/canvas/sigma.canvas.edgehovers.arrow.js"></script>
	<script src="/js/sigmajs/src/renderers/canvas/sigma.canvas.edgehovers.curvedArrow.js"></script>
	<script src="/js/sigmajs/src/renderers/canvas/sigma.canvas.extremities.def.js"></script>
	<script src="/js/sigmajs/src/renderers/svg/sigma.svg.utils.js"></script>
	<script src="/js/sigmajs/src/renderers/svg/sigma.svg.nodes.def.js"></script>
	<script src="/js/sigmajs/src/renderers/svg/sigma.svg.edges.def.js"></script>
	<script src="/js/sigmajs/src/renderers/svg/sigma.svg.edges.curve.js"></script>
	<script src="/js/sigmajs/src/renderers/svg/sigma.svg.labels.def.js"></script>
	<script src="/js/sigmajs/src/renderers/svg/sigma.svg.hovers.def.js"></script>
	<script src="/js/sigmajs/src/middlewares/sigma.middlewares.rescale.js"></script>
	<script src="/js/sigmajs/src/middlewares/sigma.middlewares.copy.js"></script>
	<script src="/js/sigmajs/src/misc/sigma.misc.animation.js"></script>
	<script src="/js/sigmajs/src/misc/sigma.misc.bindEvents.js"></script>
	<script src="/js/sigmajs/src/misc/sigma.misc.bindDOMEvents.js"></script>
	<script src="/js/sigmajs/src/misc/sigma.misc.drawHovers.js"></script>	
	<script src="/js/sigmajs/plugins/sigma.parsers.json/sigma.parsers.json.js"></script>	
	<script src="/js/sigmajs/plugins/sigma.plugins.dragNodes/sigma.plugins.dragNodes.js"></script>	
	<script src="/js/sigmajs/plugins/sigma.layout.forceAtlas2/worker.js"></script>
	<script src="/js/sigmajs/plugins/sigma.layout.forceAtlas2/supervisor.js"></script>
	<script src="/js/sigmajs/plugins/sigma.plugins.neighborhoods/sigma.plugins.neighborhoods.js"></script>
	<script src="/js/sigmajs/plugins/sigma.renderers.customShapes/shape-library.js"></script>
	<script src="/js/sigmajs/plugins/sigma.renderers.customShapes/sigma.renderers.customShapes.js"></script>
	
	<!-- Custom scripts -->
	<script src="/js/custom/scripts.js"></script>
</head>

<body>
	<!-- Заголовок -->
	<!-- <header>
		<div class="container-fluid ll-header">
			<div class="row">
				<div class="col-xs-7">
					<a class="go-home" href="/">
						<p class="text-title">Learning Lattices</p>
						<p class="text-subtitle">Lattice Learning Courses Designer</p>
					</a>
				</div>			
				<div class="col-xs-5">
					<div class="pull-right">
						<button id="open-reference" class="btn btn-default" data-toggle="modal" data-target="#reference">Справка</button>					
					</div>		
				</div>		
			</div>
		</div>
	</header> -->
	</br>
	
	<section id="big-screen">
		<!-- Основной контент -->
		<section>
			<div class="container-fluid ll-content">
				<div class="row">
					<div class="col-md-12">
						<div id="common-control" class="pull-left">
							<button id="add-course" title="Добавить курс" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></button>
							<button id="open-course" title="Открыть курс" class="btn btn-warning" data-toggle="modal" data-target="#courses-list"><span class="glyphicon glyphicon-folder-open"></span></button>
						</div>						
						<div id="nav-control" class="pull-left">
							<!-- Навигация по курсам -->
							<ul class="nav nav-tabs" id="courses-tabs"></ul>
						</div>		
						<div id="reference-control" class="pull-left">		
							<button id="open-reference" class="btn btn-info pull-right" data-toggle="modal" data-target="#reference"><span class="glyphicon glyphicon-info-sign"></span></button>
						</div>								
					</div>
					<div class="col-md-12">
						<!-- Содержимое курсов -->
						<div class="tab-content" id="courses-divs"></div>
					</div>
				</div>
			</div>
		</section>
			
		<!-- Модальное окно "Список курсов" -->
		<section>
			<div id="courses-list" class="modal fade">
				<div class="modal-content">
					<div class="modal-header">
						<div class="row">
							<div class="col-md-11">
								<h3>Список курсов</h3>
							</div>
							<div class="col-md-1">
								<button class="close" data-dismiss="modal" aria-hidden="true">Х</button>
							</div>
						</div>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-3">
								<input type="text" maxlength="256" class="form-control" placeholder="Поиск" name="search_course_name"/>
							</div>
							<div class="col-md-2">
								<select class="form-control" name="search_course_active">
									<option value="-1">Все</option>
									<option value="1">Доступные</option>
									<option value="0">Недоступные</option>
								</select>
							</div>
							<div class="col-md-2">
								<select class="form-control" name="search_course_terminal">
									<option value="-1">Все</option>
									<option value="1">Терминальные</option>
									<option value="0">Нетерминальные</option>
								</select>
							</div>
							<div class="col-md-2">
								<select class="form-control" name="search_course_content">
									<option value="-1">Все</option>
									<option value="1">С контентом</option>
									<option value="0">Без контента</option>
								</select>
							</div>
							<div class="col-md-1">		
								<button title="Обновить" class="btn btn-warning courseslist-refresh"><span class="glyphicon glyphicon-refresh"></span></button>
							</div>
							<div class="col-md-2">							
								<div class="pull-right">
									<div id="found"></div>
								</div>
							</div>
						</div>
						<div class="table-wrap">
							<table class="table table-bordered table-hover">
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<!-- Модальное окно "Справка" -->
		<section>
			<div id="reference" class="modal fade">
				<div class="modal-content">
					<div class="modal-header">
						<div class="row">
							<div class="col-md-11">
								<h3>Справка</h3>
							</div>
							<div class="col-md-1">
								<button class="close" data-dismiss="modal" aria-hidden="true">Х</button>
							</div>
						</div>
					</div>
					<div class="modal-body">
						О программе
					</div>
				</div>
			</div>
		</section>
		
		<!-- Модальное окно загрузки -->
		<section>
			<div id="loading">
				<div id="loading_gif"></div>
			</div>
		</section>
	</section>
	
	<!-- Ограничение ширины экрана 1024px -->
	<section id="small-screen">
		<p>Устройство, на котором работает система, должно иметь разрешение ширины экрана не менее, чем 1024 пикселя...</p>
	</section>
</body>

</html>