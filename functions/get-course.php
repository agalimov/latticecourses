<?php

if(!isset($_POST['allow'])) { header('Location: /403'); } /* Против вызова напрямую из браузера */

$course_id = $_POST['course_id'];
$course_name = $_POST['course_name'];

ob_start();

include 'course-tab.php'; 
$tab = ob_get_contents(); 
ob_clean();

include 'course-div.php'; 
$div = ob_get_contents();
ob_end_clean(); 

echo json_encode(array('tab' => $tab, 'div' => $div));

?>