<div class="course-div tab-pane" id="course-div-<?= $course_id ?>">

	<!-- Управление курсом -->
	<div class="course-management-wrap col-lg-6 col-md-6 col-sm-7 col-xs-7">							
		<div class="course-management" id="course-management-<?= $course_id ?>">
		
			<div class="row">
				<div class="col-xs-10">
					<input type="text" class="form-control" placeholder="* Название курса" id="i_course_name"/>
				</div>
				<div class="col-xs-2">
					<button class="btn btn-danger course-delete"><span class="glyphicon glyphicon-trash"></span></button>
				</div>
			</div>
			
			<!-- Режимы редактирования -->
			<ul class="nav nav-tabs">
				<li class="active"><a href="#course-structure-<?= $course_id ?>" data-toggle="tab">Структура</a></li>	
				<li><a href="#course-content-<?= $course_id ?>" data-toggle="tab">Контент</a></li>	
				<li><a href="#course-extension-<?= $course_id ?>" data-toggle="tab">Расширение</a></li>	
				<li><a href="#course-fragmentation-<?= $course_id ?>" data-toggle="tab">Фрагментирование</a></li>						
			</ul>
			
			<!-- Редактирование в выбранном режиме -->
			<div class="tab-content">	
			
				<!-- Структура -->
				<div class="tab-pane course-structure active" id="course-structure-<?= $course_id ?>">
					<button class="btn btn-sm btn-success add-module"><span class="glyphicon glyphicon-plus"></span></button>						
					<button class="btn btn-sm btn-primary save"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Сохранить</button>
					
					<div class="table">
					</div>		
				</div>		
				
				<!-- Контент -->
				<div class="tab-pane course-content" id="course-content-<?= $course_id ?>">
				Контент
				</div>
			
				<!-- Расширение -->
				<div class="tab-pane course-extension" id="course-extension-<?= $course_id ?>">
				Расширение
				</div>

				<!-- Фрагментирование -->
				<div class="tab-pane course-fragmentation" id="course-fragmentation-<?= $course_id ?>">
				Фрагментирование
				</div>
				
			</div>
			
			
		</div>
	</div>
	
	<!-- Граф курса -->
	<div class="course-graph-wrap col-lg-6 col-md-6 col-sm-5 col-xs-5">
		<div class="course-graph" id="course-graph-<?= $course_id ?>">
		</div>
	</div>
	
</div>