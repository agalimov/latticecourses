$(function () 
{
	graphs = [];

	// константы для построения графа
	const SIDE_MARGIN = 3;
	const MAX_NODE_SIZE = 12;
	const MAX_EDGE_SIZE = 3;
	const BORDER_SIZE = 4;
	const MIN_ARROW_SIZE = 6;
	const LABEL_THRESHOLD = 12;
	const DOUBLE_CLICK_ENABLED = false;
	const ENABLE_EDGE_HOVERING = true;
	const EDGE_HOVER_EXTREMITIES = true;
	const EDGE_HOVER_SIZE_RATIO = 2;
	
	// загрузить в список все курсы
	getCoursesList("", -1, -1, -1);
	
	// фокус на кнопках
	$("body").on("click", "button", function (e)
	{	
		$(this).blur();
	});
			
	function loading(/*opacity,*/ display) 
	{
		//$("body").css('opacity', opacity);
		$("#loading").css('display', display);
	}
			
	// загрузка курса
	function getCourse(course_id)
	{
		var tabId;
		var tabDivId;
			
		if(course_id > 0)
		{// если курс не новый, получаем элементы, к которым надо будет перейти
			tabId = "course-tab-" + course_id;
			tabDivId = "course-div-" + course_id;
		}
			
		if(!$("#courses-tabs li").is("#" + tabId) || course_id == 0)
		{// вкладка с существующим курсом ещё не открыта или курс новый		
			var resp_course_id;
						
			$.ajax({
				type:"POST",
				url:"/scripts/get-course.php", // если курс новый, то там происходит сохранение
				cache:true,
				data:{"allow":1,"course_id":course_id},
				dataType: "json",
				beforeSend: function() 
				{
					if(course_id > 0) loading('block'); // прогресс бар
				},
				success:function(responce)
				{	
					resp_course_id = responce.course_id; // актуальный id курса
					
					if(resp_course_id > 0)
					{// курс был в бд	

						// получаем элементы, к которым надо будет перейти				
						tabId = "course-tab-" + resp_course_id;
						tabDivId = "course-div-" + resp_course_id;
						
						// добавить содержимое курса
						$('#courses-tabs').append(responce.tab);
						$('#courses-divs').append(responce.div);
						
						// создаём граф курса
						graphs[resp_course_id] = new sigma({
							graph: {
								nodes: JSON.parse(responce.nodes),
								edges: JSON.parse(responce.edges)
							},						
							renderer: {
								container: document.getElementById('course-graph-' + resp_course_id),
								type: 'canvas'
							},
							settings: {
								sideMargin: SIDE_MARGIN,
								maxNodeSize: MAX_NODE_SIZE,
								maxEdgeSize: MAX_EDGE_SIZE,
								borderSize: BORDER_SIZE,
								doubleClickEnabled: DOUBLE_CLICK_ENABLED,
								enableEdgeHovering: ENABLE_EDGE_HOVERING,
								edgeHoverSizeRatio: EDGE_HOVER_SIZE_RATIO,
								edgeHoverExtremities: EDGE_HOVER_EXTREMITIES,
								labelThreshold: LABEL_THRESHOLD,
								minArrowSize: MIN_ARROW_SIZE
							}						
						});					
						
						graphs[resp_course_id].bind('doubleClickNode', function(e) { getCourse(e.data.node.id); }); // переход к курсу по двойному клику
						sigma.plugins.dragNodes(graphs[resp_course_id], graphs[resp_course_id].renderers[0]); // перетаскивание узлов
					}
					else
					{// курса не было в бд, вывести ошибку
						
					}
				}
			}).done(function()
			{
				if(resp_course_id > 0)
				{
					// отобразить вкладку и если она не в зоне видимости, проскроллить к ней
					$('#courses-tabs a[href="#' + tabDivId + '"]').tab('show');	
					$('#courses-tabs').animate({'scrollLeft': $('#'+tabId).position().left}, 800);
					
					// продвинутый граф (с картинками в узлах и проч.)
					CustomShapes.init(graphs[resp_course_id]);

					graphs[resp_course_id].refresh(); graphs[resp_course_id].refresh(); // обновить граф, для корректной работы надо 2 раза!
					
					// добавить кнопку "Домой"
					$('#course-div-' + resp_course_id + ' .course-graph').append('<div class="goto-graph"><span class="glyphicon glyphicon-home"></span></div>');	
				}
				
				loading('none'); // убрать прогресс бар			
			});
		}			
		else
		{
			// отобразить вкладку и если она не в зоне видимости, проскроллить к ней
			$('#courses-tabs a[href="#' + tabDivId + '"]').tab('show');
			$('#courses-tabs').animate({'scrollLeft': $('#'+tabId).position().left}, 800);
		}		
	}	
	
	// добавление нового курса
    $("#common-control").on("click", "#add-course", function (e)
	{					
		getCourse(0);
    });
				
	// открытие существующего курса
	$("#courses-list").on("click", ".get-course", function (e)
	{
		var course_id = $(this).attr("course-id");
		getCourse(course_id);	
	});		
	
	// список курсов
	function getCoursesList(name, active, terminal, content)
	{
		$.ajax({
			type:"POST",
			url:"/scripts/get-courses-list.php",
			cache:true,
			data:{"allow":1,"q_name":name,"q_active":active,"q_terminal":terminal,"q_content":content},
			dataType: "json",
			success:function(responce)
			{	
				$("#courses-list table tbody").html(responce.table);
				$("#courses-list #found").html(responce.cnt);
			}
		});
	}
	
	function filterCourses()
	{
		var name_val = $("#courses-list input[name='search_course_name']").val();
		var active_val = $("#courses-list select[name='search_course_active']").val();
		var terminal_val = $("#courses-list select[name='search_course_terminal']").val();
		var content_val = $("#courses-list select[name='search_course_content']").val();
		
		getCoursesList(name_val, active_val, terminal_val, content_val);
	}
	
	// фильтр курсов
	$("#courses-list").on("input", "input[name='search_course_name'], select[name='search_course_active'], select[name='search_course_terminal'], select[name='search_course_content']", function (e)
	{
		filterCourses();
	});	
	$("#courses-list").on("click", "button.courseslist-refresh", function (e)
	{
		filterCourses();
	});	
	
	// вернуть камеру на центр графа
	$("#courses-divs").on("click", ".goto-graph", function (e)
	{	
		var course_id = $(this).closest(".course-div").data("course-id");
		
		sigma.misc.animation.camera(
			graphs[course_id].camera, 
			{
				x: 0, y: 0, ratio: 1
			}, 
			{duration: 300}
		);
		//graphs[course_id].camera.goTo({x: 0, y: 0});
	});
	
	// переход по курсам
    $("#courses-tabs").on("click", "a", function (e) 
	{
        e.preventDefault();		
        $(this).tab('show');
    });	
	$("#courses-tabs").on('shown.bs.tab', "a", function (e)
	{	
	});
	
	// закрытие курса
	$("#courses-tabs").on("click", ".close", function (e) 	
	{
        e.preventDefault();
						
        var tabContentId = $(this).parent().attr("href");
		
		var course_id = tabContentId.replace("#course-div-", "");	
		
		graphs[course_id].kill(); // удалить граф и все его объекты						
		delete graphs[course_id]; // удалить ссылку на граф
		
		// переход к соседнему курсу
		var tabLi = $(this).closest("li");
		if(tabLi.hasClass("active"))
		{
			tabLi.next().length > 0 ? tabLi.next().find("a").tab('show') : tabLi.prev().find("a").tab('show');
        }
		
		$(this).parent().parent().remove(); // remove tab
        $(tabContentId).remove(); // remove div
    });
		
	// добавление подкурса
	$("#courses-divs").on("click", ".add-subcourse", function (e) 	
	{
		var subcourses_table = $(this).closest(".course-management").find(".course-structure .table tbody"); // таблица подкурсов
			
		if(!subcourses_table.find('tr').is(".new"))
		{// если новый подкурс не в режиме редактирования			
			$.ajax({
				type:"POST",
				url:"/scripts/get-subcourse.php",
				cache:false,
				data:{"allow":1},
				dataType: "json",
				success:function(responce)
				{	
					subcourses_table.prepend(responce.subcourse);
					subcourse_data = subcourses_table.find('.subcourse:first .subcourses-info>.row');
					subcourse_data.show(600);			
					subcourse_data.find("input[name='i_subcourse_name']").focus();
				}
			});			
		}
		else
		{// новый подкурс ещё редактируется
			subcourses_table.find("tr.new input[name='i_subcourse_name']").focus();
		}		
	});
	
	
	// добавление подкурса
	$("#courses-divs").on("click", ".course-refresh", function (e) 	
	{
		//console.log(resp_course_id);
	});
	
	
	// открыть подкурс как курс
	$("#courses-divs").on("click", ".get-course", function (e)
	{		
		var course_id = $(this).closest("tr").data("subcourse-id"); 
		getCourse(course_id);		
    });	
		
	// сохранение подкурса после ввода его названия
	$("#courses-divs").on("change", "input[name='i_subcourse_name']", function (e)
	{
		var subcourses_table = $(this).closest(".table tbody");
		var course_name = $(this).val();
		var course_id = $(this).closest(".course-div").data('course-id');
		var subcourses_count = $(this).closest(".course-management").find(".badge.subc-count");
	
		$.ajax({
			type:"POST",
			url:"/scripts/add-subcourse.php",
			cache:false,
			data:{"allow":1,"course_id":course_id,"course_name":course_name},
			dataType: "json",
			success:function(responce)
			{
				subcourses_table.find('.subcourse:first').remove(); // удаление строки с новым подкурсом ...
				
				subcourses_count.html(subcourses_table.find('tr').length + 1); // количество подкурсов увеличилось
				
				// вставить в каждый datalist новую запись
				subcourses_table.find("datalist").each(function() {
					$(this).prepend('<option data-source-id="' + responce.subcourse_id + '" value="' + responce.subcourse_name + '">');
				});
				
				subcourses_table.prepend(responce.subcourse); // и вставка на его место того же подкурса, но как сохранённого 
			}
		});
	});	
	
	// удаление подкурса
	$("#courses-divs").on("click", ".delete-subcourse", function (e) 	
	{
		var subcrs = $(this).closest('.subcourse'); // строка tr с подкурсом
		var subcrs_in = subcrs.find('.subcourses-info>.row'); // содержимое (т.к. с tr анимация работает иначе)
		var subcourse_id = subcrs.data('record-subcourse-id');

		$.ajax({
			type:"POST",
			url:"/scripts/delete-subcourse.php",
			cache:false,
			data:{"allow":1,"subcourse_id":subcourse_id},
			success:function(responce)
			{
				subcrs_in.hide(400, function() // анимированно спрятать ...
				{
					subcrs.remove(); // ... и удалить
				});		
			}				
		});	
	});	
		
	// удаление всех подкурсов
	$("#courses-divs").on("click", ".clear-structure", function (e) 	
	{
		$(this).closest(".course-structure").find(".table tr").each(function() {
			$(this).hide(400, function() // анимированно спрятать ...
			{
				$(this).remove(); // ... и удалить
			});
		});		
	});
	
	// добавление связи подкурса
	$("#courses-divs").on("input", ".links-control input[name='i_subcourse_link']", function (e) 	
	{		
		var input = $(this);
		var input_val = $(this).val();
		var datalist = $(this).siblings("datalist");
		var subcourse = $(this).closest("tr.subcourse");
		
		if(datalist.find('option').filter(function(){
			return $(this).val() === input_val; // функция ниже срабатывает только тогда, когда есть полное совпадение названия курса (т.е. при вводе или при выборе)       
		}).length) {
			
			// получить источник и назначение
			var source_id = datalist.find("option[value='" + input_val + "']").data('source-id');
			var destination_id = subcourse.data("subcourse-id");
						
			if(!subcourse.find('.link-item').is('[data-source-id="' + source_id + '"]')) // если такая связь ещё не создана
			{
				$.ajax({
					type:"POST",
					url:"/scripts/add-course-link.php",
					cache:false,
					data:{"allow":1,"source_id":source_id,"destination_id":destination_id},
					dataType: "json",
					success:function(responce)
					{
						var links = input.closest('.subcourses-info').find('.links-list');
						
						if(links.find('.link-item').length)
							links.find('.link-item:last-child').after(responce.course_link);
						else
							links.append(responce.course_link);
						
						subcourse.find('.link-item[data-source-id="' + source_id + '"]').show(600);
						
						subcourse.find('.subcourse-delclear .clear-subcourse').show();
					}				
				});
			}
			else
			{// проинформировать, что связь уже есть
				/**/
			}
			
			input.val(''); // очистить введённое значение
		}
	});
	
	// удаление связи подкурса
	$("#courses-divs").on("click", ".delete-link", function (e) 	
	{		
		var lnk = $(this).closest(".link-item");
		var lnk_id = lnk.data('link-id');
		
		$.ajax({
			type:"POST",
			url:"/scripts/delete-course-link.php",
			cache:false,
			data:{"allow":1,"link_id":lnk_id},
			success:function(responce)
			{
				lnk.hide(400, function() // анимированно спрятать ...
				{
					if(!lnk.siblings('.link-item').length)	// убрать кнопку "Очистить связи", если их не осталось			
						lnk.closest(".subcourses-info").find('.subcourse-delclear .clear-subcourse').hide();
					
					lnk.remove(); // ... и удалить
				});	
			}				
		});	
	});
	
	// удаление всех связей подкурса
	$("#courses-divs").on("click", ".clear-subcourse", function (e) 	
	{
		var lnk_list = $(this).closest(".subcourse").find(".links-list");
		var lnk_ids = lnk_list.find(".link-item").map(function() {
		  return $(this).data('link-id');
		}).get();
		
		$.ajax({
			type:"POST",
			url:"/scripts/delete-all-course-links.php",
			cache:false,
			data:{"allow":1,"link_ids":JSON.stringify(lnk_ids)},
			success:function(responce)
			{
				lnk_list.hide(400, function() // анимированно спрятать ...
				{
					lnk_list.html("");  // ... очистить контент ...
					lnk_list.show(); // ... и снова показать (уже пустой)
				});		
			}				
		});		
	});
	
	// Достраивание введённой структуры до алгебраической решётки
	$("#courses-divs").on("click", ".course-to-lattice", function (e) 	
	{
		var course_id = $(this).closest(".course-div").data('course-id');
		
		$.ajax({
			type:"POST",
			url:"/scripts/course-to-lattice.php",
			cache:false,
			data:{"allow":1,"course_id":course_id},
			dataType: "json",
			beforeSend: function() 
			{
				loading('block'); // прогресс бар
			},
			success:function(responce)
			{
				graphs[course_id].graph.clear();
				
				var errors = JSON.parse(responce.errors);
				var array_errors = Object.values(errors);
				
				if(array_errors.length)
				{
					for(var i=0;i<array_errors.length;i++)
					{
						alert(array_errors[i]);
					}					
				}
				else
				{
					graphs[course_id].graph.read({
					  nodes: JSON.parse(responce.nodes),
					  edges: JSON.parse(responce.edges)
					});
				}
			}				
		}).done(function()
		{				
			graphs[course_id].refresh();			
									
			loading('none');			
		});
	});
	
	/*$('.document').bind("DOMSubtreeModified", hideMsg);function hideMsg(){
    // Hide dynamically added div
    setTimeout(function(){ 
          $('#result').slideUp(500);  
    }, 5000);*/
});