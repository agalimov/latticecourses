var composeCount = 0;

// initilize tabs
$(function () 
{
    // when ever any tab is clicked this method will be call
    $("#courses-tabs").on("click", "a", function (e) {
        e.preventDefault();		
        $(this).tab('show');
    });
	
    $('#add_course').click(function (e) 
	{		
		e.preventDefault();
		
		if(!$("#courses-tabs li").is("#course-tab-0"))
		{
			var tabId = "course-tab-0";
			var tabContId = "course-div-0";
			
			$.ajax({
				type:"POST",
				url:"/functions/getCourse.php",
				cache:false,
				data:{"allow":1,"course_id":0},
				dataType: "json",
				success:function(responce)
				{
					alert(responce.tab);
					alert(responce.div);

					/*$('#courses-tabs').append(responce.tab);
					$('#courses-divs').append(responce.div);
					$('#courses-tabs a[href="#course-div-0"]').tab('show');*/
				}
			});
		}
    });
	
	$("#courses-tabs").on("click", ".close", function (e) 	
	{
        e.preventDefault();
		
        // there are multiple elements in #courses-tabswhich has .close icon so close the tab whose close icon is clicked
        var tabContentId = $(this).parent().attr("href");
		var tabLi = $(this).closest("li");
		if(tabLi.hasClass("active"))
		{
			tabLi.next().length > 0 ? tabLi.next().find("a").tab('show') : tabLi.prev().find("a").tab('show');
        }
		$(this).parent().parent().remove(); // remove li of tab
        $(tabContentId).remove(); // remove respective tab content
    });
});