var currentTab;
var composeCount = 0;

// initilize tabs
$(function () 
{
    // when ever any tab is clicked this method will be call
    $("#courses-tabs").on("click", "a", function (e) {
        e.preventDefault();
		
        $(this).tab('show');
        $currentTab = $(this);
    });

    registerComposeButtonEvent();
    registerCloseEvent();
});

// this method will demonstrate how to add tab dynamically
function registerComposeButtonEvent() 
{
    $('#composeButton').click(function (e) 
	{
        e.preventDefault();
		var tabId = "course-tab-" + composeCount;
		var tabContId = "course-div-" + composeCount;

        $('#courses-tabs').append('<li id="' + tabId + '" class="course-tab"><a href="#' + tabContId + '"><button class="close" type="button" >×</button>Compose</a></li>');
        $('#courses-divs').append('<div class="course-div tab-pane" id="' + tabContId + '"></div>');

        //createNewTabAndLoadUrl("", "./SamplePage.html", "#" + composeCount);

        //$(this).tab('show');
        showTab(tabContId);
        registerCloseEvent();		
		
        composeCount = composeCount + 1; // increment compose count
    });
}

// this method will register event on close icon on the tab..
function registerCloseEvent() 
{
    $("#courses-tabs .close").click(function (e) 
	{
        e.preventDefault();
		
        // there are multiple elements in #courses-tabswhich has .close icon so close the tab whose close icon is clicked
        var tabContentId = $(this).parent().attr("href");
		var tabLi = $(this).closest("li");
		if(tabLi.hasClass("active"))
		{
			tabLi.next().length > 0 ? tabLi.next().find("a").tab('show') : tabLi.prev().find("a").tab('show');
        }
		$(this).parent().parent().remove(); // remove li of tab
        $(tabContentId).remove(); // remove respective tab content
    });
}

// shows the tab with passed content div id..paramter tabcontid indicates the div where the content resides
function showTab(tabContId) 
{
    $('#courses-tabs a[href="#' + tabContId + '"]').tab('show');
}
// return current active tab
function getCurrentTab() 
{
    return currentTab;
}

// this function will create a new tab here and it will load the url content in tab content div.
function createNewTabAndLoadUrl(parms, url, loadDivSelector) 
{
    $("" + loadDivSelector).load(url, function (response, status, xhr) {
        if (status == "error") {
            var msg = "Sorry but there was an error getting details ! ";
            $("" + loadDivSelector).html(msg + xhr.status + " " + xhr.statusText);
            $("" + loadDivSelector).html("Load Ajax Content Here...");
        }
    });
}




// this will return element from current tab
// example : if there are two tabs having  textarea with same id or same class name then when $("#someId") whill return both the text area from both tabs
// to take care this situation we need get the element from current tab.
function getElement(selector) 
{
    var tabContentId = $currentTab.attr("href");
    return $("" + tabContentId).find("" + selector);
}

function removeCurrentTab() 
{
    var tabContentId = $currentTab.attr("href");
    $currentTab.parent().remove(); // remove li of tab
    $('#courses-tabs a:last').tab('show'); // select first tab
    $(tabContentId).remove(); // remove respective tab content
}