<?php if(!isset($_POST['allow'])) { header('Location: /403'); } /* Против вызова напрямую из браузера */ ?>
<tr class="subcourse<?php if(!isset($subcourse)) echo ' new'; ?>">
	<td class="subcourses-info">
		<input type="hidden" name="i_subcourse_id" value="<?= $subcourse['id'][0] /* [0] - id подкурса как записи таблицы subcourses, [1] - id подкурса как курса (запись в таблице courses) */ ?>"/>
		<div class="row">
			<div class="col-xs-7">
				<input type="text" maxlength="256" class="form-control" placeholder="* Название подкурса" name="i_subcourse_name" value="<?= $subcourse['name'] ?>"/>
			</div>
			<div class="col-xs-5">		
				<div class="prev-subcourse-link pull-right" title="Выберите элемент связи слева">+</div>
				<input list="subcourse-<?= $subcourse['id'][0] ?>-link" class="form-control pull-right subcourse-link" name="i_subcourse_link"/>
				<datalist id="subcourse-<?= $subcourse['id'][0] ?>-link">
					<?php foreach($course['subcourses'] as $sc) { ?>
						<?php if($sc['id'][0] != $subcourse['id'][0]) { ?>	
							<option id="<?= $sc['id'][0] ?>" value="<?= $sc['name'] ?>">
						<?php } ?>
					<?php } ?>
				</datalist>
			</div>
		</div>	
		<div class="links-list">
			<?php foreach($subcourse['links'] as $sc_links) { ?>		
				<div class="link-item" title="<?= $sc_links['name'] ?>">
					<div><?= $sc_links['name'] ?></div>
					<a title="Удалить связь" class="delete-link pull-right"><span class="glyphicon glyphicon-remove"></span></a>
				</div>
			<?php } ?>	
		</div>
	</td>
	<td class="subcourses-tools">									
		<button title="Удалить подкурс" class="btn btn-danger delete-subcourse"><span class="glyphicon glyphicon-remove"></span></button>
		<button title="Перейти к редактированию" class="btn btn-warning open-course-by-id"><span class="glyphicon glyphicon-new-window"></span></button>	
	</td>
</tr>