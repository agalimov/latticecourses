<?php if(!isset($_POST['allow'])) { header('Location: /403'); } /* Против вызова напрямую из браузера */ ?>
<div class="course-div tab-pane" id="course-div-<?= $course['id'] ?>">

	<!-- Управление курсом -->
	<div class="course-management-wrap col-lg-6 col-md-6 col-sm-7 col-xs-7">							
		<div class="course-management">
			<input type="hidden" name="i_course_id" value="<?= $course['id'] ?>"/>
		
			<div class="row">
				<div class="col-xs-9">
					<input type="text" maxlength="256" class="form-control" placeholder="* Название курса" name="i_course_name" value="<?= $course['name'] ?>"/>
				</div>
				<div class="col-xs-3">
					<div class="pull-right">
						<button title="Сохранить курс" class="btn btn-primary course-save"><span class="glyphicon glyphicon-floppy-disk"></span></button>
						<button title="Удалить курс" class="btn btn-danger course-delete"><span class="glyphicon glyphicon-trash"></span></button>
					</div>
				</div>
			</div>
			
			<!-- Режимы редактирования -->
			<ul class="nav nav-tabs">
				<li class="active"><a href="#course-structure-<?= $course['id'] ?>" data-toggle="tab">Структура</a></li>	
				<li><a href="#course-links-<?= $course['id'] ?>" data-toggle="tab">Связи</a></li>		
				<li><a href="#course-content-<?= $course['id'] ?>" data-toggle="tab">Контент</a></li>	
				<li><a href="#course-extension-<?= $course['id'] ?>" data-toggle="tab">Расширение</a></li>	
				<li><a href="#course-fragmentation-<?= $course['id'] ?>" data-toggle="tab">Фрагментирование</a></li>						
			</ul>
			
			<!-- Редактирование в выбранном режиме -->
			<div class="tab-content">	
			
				<!-- Структура -->
				<div class="tab-pane course-structure active" id="course-structure-<?= $course['id'] ?>">				
					<button title="Добавить подкурс" class="btn btn-success add-subcourse"><span class="glyphicon glyphicon-plus"></span></button>			
					<button class="btn btn-primary course-to-lattice"><span class="glyphicon glyphicon-pencil"></span>&nbsp;Достроить до решётки</button>
					<button class="btn btn-info show-course"><span class="glyphicon glyphicon-search"></span>&nbsp;Предварительный просмотр</button>		
					
					<div class="table-wrap">
						<table class="table table-bordered">
							<tbody>
								<?= $subcourses_html ?>
							</tbody>
						</table>
					</div>	
				</div>		
				
				<!-- Контент -->
				<div class="tab-pane course-content" id="course-content-<?= $course['id'] ?>">
				Контент
				</div>
				
				<!-- Связи -->
				<div class="tab-pane course-links" id="course-links-<?= $course['id'] ?>">
				Связи
				</div>
			
				<!-- Расширение -->
				<div class="tab-pane course-extension" id="course-extension-<?= $course['id'] ?>">
				Расширение
				</div>

				<!-- Фрагментирование -->
				<div class="tab-pane course-fragmentation" id="course-fragmentation-<?= $course['id'] ?>">
				Фрагментирование
				</div>
				
			</div>
			
		</div>
	</div>
	
	<!-- Граф курса -->
	<div class="course-graph-wrap col-lg-6 col-md-6 col-sm-5 col-xs-5">
		<div class="course-graph" id="course-graph-<?= $course['id'] ?>">
		</div>
	</div>
	
</div>