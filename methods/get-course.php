<?php

if(!isset($_POST['allow'])) { header('Location: /403'); } /* Против вызова напрямую из браузера */

include '../database/config.php';
include '../database/db.php';

$course_id = $_POST['course_id'];

dbconnect();
$course = getCourse($course_id);
dbclose();

ob_start();

if($course['subcourses'])
{
	foreach($course['subcourses'] as $subcourse)
	{		
		include '/get-subcourse/subcourse.php'; 
		$subcourses_html = ob_get_contents(); 
	}
}
ob_clean();

include '/get-course/course-tab.php'; 
$tab = ob_get_contents(); 
ob_clean();

include '/get-course/course-div.php'; 
$div = ob_get_contents();
ob_end_clean(); 

echo json_encode(array('tab' => $tab, 'div' => $div));

?>