<?php

    $db_con = null;
	
	define(NEW_COURSE_NAME, '[*новый*]');
	define(NEW_MIN_SUBCOURSE_NAME, 'Ø_');
	define(NEW_MAX_SUBCOURSE_NAME, 'I_');
	define(NEW_ADDED_SUBCOURSE_NAME, 'ADDED');
	define(NODE_SIZE, 12);
	define(NODE_COLOR_TERMINAL, '#4876ff');
	define(NODE_COLOR_NOTERMINAL, '#d9534f');
	define(NODE_COLOR_ADDED, '#5cb85c');
	define(NODE_IMAGE_CONTENT, 'storage/images/additional/content.png');
	define(NODE_IMAGE_MIN, 'storage/images/additional/minimal.png');
	define(NODE_IMAGE_MAX, 'storage/images/additional/maximal.png');
	define(EDGE_SIZE, 3);
	define(EDGE_TYPE, 'arrow');
	define(EDGE_COLOR, '#ccc');	
	define(X_STEP, 12);
	define(Y_STEP, 12);

    /* соединение с бд */
    function dbconnect()
    {
        ini_set('default_charset', 'UTF-8');
        global $DB_DATABASE, $DB_HOST, $DB_USER, $DB_PASSWORD, $db_con;
        $db_con = new PDO('mysql:dbname='.$DB_DATABASE.';host='.$DB_HOST.';charset=utf8', $DB_USER, $DB_PASSWORD);
        $db_con->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $db_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $db_con;
    }

    /* закрытие соединения */
    function dbclose()
	{
        global $db_con;
        $db_con = null;
    }	
	
    /* получить таблицу с префиксом */
	function table($table)
	{
        global $DB_PREFIX;
		return $DB_PREFIX.$table;
	}
	
	/* получить контент курса */
	function getCourseContent($course_id)
	{
        global $db_con;
		$query = 'SELECT * FROM '.table('courses_contents').' WHERE course_id = ?';
		$sql = $db_con->prepare($query);
		$sql->execute(array($course_id));
		$contents = $sql->fetchAll(PDO::FETCH_NAMED);
		return $contents;
	}
	
	/* получить связи курса (соединение с таблицей курсов для получения названия каждого из них) */
    function getCourseLinks($course_id, $to = false)
	{
        global $db_con;		
		$query = 'SELECT '.table('courses_links').'.*, '.table('courses').'.id, '.table('courses').'.name FROM '.table('courses_links').' INNER JOIN '.table('courses').' ON '.table('courses_links').'.'.($to ? 'dest_id' : 'src_id').' = '.table('courses').'.id AND '.table('courses_links').'.'.($to ? 'src_id' : 'dest_id').' = ? AND '.table('courses').'.active = 1';
		$sql = $db_con->prepare($query);
		$sql->execute(array($course_id));
		$links = $sql->fetchAll(PDO::FETCH_NAMED);
		return $links;
	}
		
	/* добавить связь курсов */
	function addCourseLink($source_id, $destination_id)
	{
        global $db_con;
		$query = 'INSERT INTO '.table('courses_links').'(src_id,dest_id) VALUES(?,?)';
		$sql = $db_con->prepare($query);
		$sql->execute(array($source_id, $destination_id));
        $link_id = $db_con->lastInsertId();
		
		return $link_id;
	}
	
	/* получить связь курсов (соединение с таблицей курсов для получения названия каждого из них) */
	function getCourseLink($link_id)
	{
        global $db_con;
		$query = 'SELECT '.table('courses_links').'.*, '.table('courses').'.id, '.table('courses').'.name FROM '.table('courses_links').' INNER JOIN '.table('courses').' ON '.table('courses_links').'.src_id = '.table('courses').'.id AND '.table('courses_links').'.id = ? AND '.table('courses').'.active = 1';
		$sql = $db_con->prepare($query);
		$sql->execute(array($link_id));
		$link = $sql->fetch(PDO::FETCH_NAMED);
		
		return $link;
	}
	
	/* получить связи всех подкурсов курса */
	function getAllSubcoursesLinks($course_id)
	{
        global $db_con;
		$query = 
		'SELECT * FROM '.table('courses_links').' WHERE '.table('courses_links').'.src_id IN (SELECT subcourse_id FROM '.table('subcourses').' WHERE course_id = ?) AND '.table('courses_links').'.dest_id IN (SELECT subcourse_id FROM '.table('subcourses').' WHERE course_id = ?)';
		$sql = $db_con->prepare($query);
		$sql->execute(array($course_id, $course_id));
		$links = $sql->fetchAll(PDO::FETCH_NAMED);
		
		return $links;
	}
	
	/* удалить связь по id */
    function deleteCourseLinkById($link_id) 
	{
        global $db_con;
		$query = 'DELETE FROM '.table('courses_links').' WHERE id = ?';
        $sql = $db_con->prepare($query);
        $sql->execute(array($link_id));
    }
	
	/* удалить связь по элементам */
    function deleteCourseLink($src_id, $dest_id) 
	{
        global $db_con;
		$query = 'DELETE FROM '.table('courses_links').' WHERE src_id = ? AND dest_id = ?';
        $sql = $db_con->prepare($query);
        $sql->execute(array($src_id, $dest_id));
    }
		
	/* получить связи курса (только id`s элементов-предков и элементов-потомков) */
	function getCourseLinksFromToIds($links_from, $links_to)
	{
		$links = [];
		foreach($links_from as $link_from) 
		{
			$links['from'][] = $link_from['src_id']; // связи ОТ к элементов-предков 
		}	
		foreach($links_to as $link_to)
		{
			$links['to'][] = $link_to['dest_id']; // связи К элементам-потомкам
		}
		
		return $links;
	}
		
	/* получить подкурсы курса (соединение с таблицей курсов для получения названия каждого из них + сортировка по количеству связей) */
	function getCourseSubcourses($course_id)
	{		
        global $db_con;
		$query = 'SELECT '.table('courses').'.id, '.table('courses').'.name, '.table('subcourses').'.*, COUNT('.table('courses_links').'.id) AS count_links_from_other 
				FROM '.table('subcourses').' 
				INNER JOIN '.table('courses').' ON '.table('subcourses').'.subcourse_id = '.table('courses').'.id AND '.table('subcourses').'.course_id = ? AND '.table('courses').'.active = 1
				LEFT JOIN '.table('courses_links').' ON '.table('courses_links').'.dest_id = '.table('courses').'.id
				GROUP BY '.table('subcourses').'.id ORDER BY count_links_from_other ASC';
				
		/*$query = 'SELECT * FROM '.table('subcourses').' INNER JOIN '.table('courses').' ON '.table('subcourses').'.subcourse_id = '.table('courses').'.id AND '.table('subcourses').'.course_id = ? AND '.table('courses').'.active = 1';*/
		
		$sql = $db_con->prepare($query);
		$sql->execute(array($course_id));
		$subcourses = $sql->fetchAll(PDO::FETCH_NAMED);
		
		foreach($subcourses as &$sc)
		{
			$sc['links'] = getCourseLinks($sc['subcourse_id']);
		}
		unset($sc);
		
		return $subcourses;
	}
		
	/* получить подкурс курса по id записи в таблице subcourses (соединение с таблицей курсов для получения названия каждого из них + сортировка по количеству связей) */
	function getCourseSubcourse($subcourse_rec_id)
	{		
        global $db_con;
		$query = 'SELECT '.table('courses').'.id, '.table('courses').'.name, '.table('subcourses').'.*, COUNT('.table('courses_links').'.id) AS count_links_from_other 
				FROM '.table('subcourses').' 
				INNER JOIN '.table('courses').' ON '.table('subcourses').'.id = ? AND '.table('subcourses').'.subcourse_id = '.table('courses').'.id AND '.table('courses').'.active = 1
				LEFT JOIN '.table('courses_links').' ON '.table('courses_links').'.dest_id = '.table('courses').'.id
				GROUP BY '.table('subcourses').'.id';
		
		$sql = $db_con->prepare($query);
		$sql->execute(array($subcourse_rec_id));
		$subcourse = $sql->fetch(PDO::FETCH_NAMED);
		
		$subcourse['links'] = getCourseLinks($subcourse['subcourse_id']);
		
		return $subcourse;
	}

	/* имеет ли курс подкурсы */
    function courseHasSubcourses($course_id)
	{
		global $db_con;
		$query = 'SELECT id FROM '.table('subcourses').' WHERE course_id = ? LIMIT 1';
		$sql = $db_con->prepare($query);
		$sql->execute(array($course_id));		
		$res = $sql->fetch(PDO::FETCH_ASSOC);
		
		if(!is_null($res) && !empty($res)) 
			return true; 
		else 
			return false;
	}
	
	/* имеет ли курс контент */
    function courseHasContent($course_id)
	{
		global $db_con;
		$query = 'SELECT id FROM '.table('courses_contents').' WHERE course_id = ? LIMIT 1';
		$sql = $db_con->prepare($query);
		$sql->execute(array($course_id));		
		$res = $sql->fetch(PDO::FETCH_ASSOC);
		
		if(!is_null($res) && !empty($res)) 
			return true; 
		else 
			return false;
	}
	
	/* получить главный фильтр (всех потомков) элемента */
	function getSubcoursesFilters(&$filters, $links_to, &$subcourses_ids, &$subcourses_layers, $current_layer, $need_set, &$err)
	{		
		// необходимо для работы функций merge и unique		
		if(is_null($links_to))
			$links_to = array();
			
		if(is_null($filters))
			$filters = array();
					
		if(is_null($subcourses_layers[$current_layer]))
			$subcourses_layers[$current_layer] = array();
					
		if($need_set)
		{// нужно настраивать слои и проверять структуру на корректность
	
			foreach($links_to as $link)
			{// настраиваем слои
				
				if($subcourses_ids[$link]['layer'] < $current_layer)
				{// рассматривать элемент, если он расположен ниже текущего
			
					if($subcourses_ids[$link]['layer'] > 0)
					{// если элементу уже назначался слой, то удалить его из этого слоя (далее в коде назначается новый слой)
							
						for($i=0; $i<count($subcourses_layers[$subcourses_ids[$link]['layer']]); $i++) // ищем удаляемый элемент среди всех элементов слоя
						{
							if($subcourses_layers[$subcourses_ids[$link]['layer']][$i] == $link)
							{
								array_splice($subcourses_layers[$subcourses_ids[$link]['layer']], $i, 1); // и удаляем
							}
						}
					}
					
					// назначаем слой элементу 
					$subcourses_ids[$link]['layer'] = $current_layer; 
				
					// и элемент слою
					if(!in_array($link, $subcourses_layers[$current_layer]))
					{
						$subcourses_layers[$current_layer][] = $link; 
					}
				}
			}
		
			foreach($links_to as $link)
			{// проверяем структуру	на остуствие замыканий
			
				if(!is_null($subcourses_ids[$link]['links_to']))
				{				
					foreach($subcourses_ids[$link]['links_to'] as $l_to)
					{// перебрать элементы-потомки текущей связи
					
						if($subcourses_ids[$link]['layer'] > $subcourses_ids[$l_to]['layer'] && $subcourses_ids[$l_to]['layer'] >= 0)
						{//... и если они расположены ниже, то выдать ошибку
							$err[$link.'-'.$l_to] = 'Связь "'.$subcourses_ids[$link]['name'].' &rarr; '.$subcourses_ids[$l_to]['name'].'" образует замыкание!';
							/*break;*/
						}
					}
					
					/*if(!empty($err))
					{// прекратить перебор остальных связей
						break;
					}*/
				}
			}
		}
		
		if(empty($err))
		{
			// добавляем в фильтр минимального элемента связи на элементы-потомки
			$filters = array_unique(array_merge($filters, $links_to));
			
			foreach($links_to as $link)
			{			
				getSubcoursesFilters($filters, $subcourses_ids[$link]['links_to'], $subcourses_ids, $subcourses_layers, $current_layer+1, $need_set, $err);
			}
		}
	}
	
	/* получить граф курса */
	function getCourseGraph($course_id, $subcourses)
	{	
		$nodes = []; // узлы
		$edges = []; // и ребра графа
		
		$subcourses_assoc_by_id = []; // ключ - id подкурса, значение - массив: [массив связей К от других, массив связей ОТ к другим, название, номер слоя]
		$subcourses_assoc_by_layer = []; // ключ - номер слоя, значение - массив id`s элементов
		$errors = []; // ошибки в структуре курса

		
		// 1 // Извлекаем информацию о всех элементах введённой структуры в массив subcourses_assoc_by_id
		foreach($subcourses as &$subcourse)
		{
			$subcourse['links_to'] = getCourseLinks($subcourse['id'][0], true); // получить связи К элементам-потомкам			
			$links = getCourseLinksFromToIds($subcourse['links'], $subcourse['links_to']); // получить связи в виде двух массивов с id`s элементов
			
			if(empty($links['from']) && empty($links['to']))
			{// элемент не имеет никаких связей
				$errors[] = 'Элемент "'.$subcourse['name'].'" не имеет связей!';
			}
			else
			{// если ошибок нет, продолжать формировать массив
				$subcourses_assoc_by_id[$subcourse['id'][0]] = ['links_from' => $links['from'], 'links_to' => $links['to'], 'name' => $subcourse['name'], 'layer' => -1, 'added' => false];			
			}
		}
		unset($subcourse);
		unset($links);
		unset($subcourses);

		if(empty($errors))
		{// связи есть у всех элементов
						
			// 2 // Получаем минимальные узлы (не имеющие связей от других узлов) 
			foreach($subcourses_assoc_by_id as $id => &$subcourse)
			{
				if(empty($subcourse['links_from']))
				{// выбираем элементы без предков

					$subcourse['layer'] = 0; 				// назначаем первый слой
					$subcourses_assoc_by_layer[0][] = $id;  // добавляем элемент в первый слой
				}
				else { break; }	// как только элементы без предков заканчиваются, идём дальше
			}
			unset($subcourse);
			
			
			// 3 // Назначаем каждому элементу слой (для y-координаты)

			// работа начинается с минимального слоя, после расчёта фильтров элементов которого становятся известны слои ВСЕХ элементов структуры. Здесь же проверяется наличие во введённой структуре замыканий. Расчёт фильтров остальных элементов выполняется отдельно 

			// подготавливаем полученные элементы 
			if(empty($subcourses_assoc_by_layer[0]))
			{
				$errors[] = 'В структуре отсутствуют минимальные элементы!';
			}
			else
			{
				$layer_subcourses_filters = []; // массив главных фильтров элементов слоя (id`s ВСЕХ потомков)
				foreach($subcourses_assoc_by_layer[0] as $min_subcourse_id)
				{
					// проверяем структуру на наличие замыканий и ищем ФИЛЬТРЫ для элементов МИНИМАЛЬНОГО СЛОЯ и обновляем НОМЕРА СЛОЁВ ВСЕХ элементов (subcourses_assoc_by_id) и ЭЛЕМЕНТОВ В этих СЛОЯХ (subcourses_assoc_by_layer)
					getSubcoursesFilters($layer_subcourses_filters[$min_subcourse_id], $subcourses_assoc_by_id[$min_subcourse_id]['links_to'], $subcourses_assoc_by_id, $subcourses_assoc_by_layer, 1, true, $errors);	
					
					/*if(!empty($errors)) break;*/		
				}
			}

			// здесь необходимо убирать последний слой, но это делается далее перед шагом 5.3, поскольку здесь по какой-то причине не отрабатывает
				
			if(empty($errors))
			{// минимальные элементы присутсвуют

				// расчёт фильтров всех остальных элементов (проходим по элементам послойно)
				foreach($subcourses_assoc_by_layer as $num_layer => $layer_subcourses)
				{
					if($num_layer > 0)
					{// не рассматривать вновь минимальные элементы
				
						foreach($layer_subcourses as $ls)
						{
							getSubcoursesFilters($layer_subcourses_filters[$ls], $subcourses_assoc_by_id[$ls]['links_to'], $subcourses_assoc_by_id, $subcourses_assoc_by_layer, $num_layer+1, false, $errors);
						}
					}			
				}
				
				
				// 4 // Найдём всевозможные пересечения фильтров БЕЗ ПОВТОРЕНИЙ
				
				// составляем всевозможные пары элементов, без указаний на самих себя и повторений (т.е. не 1-2, 2-1, 1-1, 2-2, а только 1-2)
				$layer_subcourses_pairs = []; // массив пар элементов (id`s)
				foreach($subcourses_assoc_by_id as $k1=>$ls1)
				{
					foreach($subcourses_assoc_by_id as $k2=>$ls2)
					{
						if($k1 != $k2 && !in_array([$k2, $k1], $layer_subcourses_pairs))
						{
							$layer_subcourses_pairs[] = [$k1, $k2];
						}
					}
				}
			
				// выполняем для каждой пары элементов (в одном слое) следующие действия
				$filter_intersections = [];
				foreach($layer_subcourses_pairs as $pairs)
				{
					// находим пересечение фильтров элементов pairs[0] и pairs[1]
					$filter_intersection = array_intersect($layer_subcourses_filters[$pairs[0]], $layer_subcourses_filters[$pairs[1]]);
					
					// по умолчанию фильтр ещё не добавлялся
					$exists = false;
					
					// проверяем, добавлялось ли уже такое пересечение фильтров
					foreach($filter_intersections as &$fi)
					{
						if(empty(array_diff($filter_intersection, $fi['intersection'])) && empty(array_diff($fi['intersection'], $filter_intersection)))
						{// если такой фильтр уже есть
										
							if(min($pairs) != $fi['elem1'] || max($pairs) != $fi['elem2'])
							{// но элементы-источники пересечения фильтра другие
								
								if(min($subcourses_assoc_by_id[min($pairs)]['layer'], $subcourses_assoc_by_id[max($pairs)]['layer']) 
									> 
								   min($subcourses_assoc_by_id[$fi['elem1']]['layer'], $subcourses_assoc_by_id[$fi['elem2']]['layer']))
								{// сохранить для фильтра элементы-источники с наибольшим (наивысшим) слоем
								
									$fi['elem1'] = min($pairs);
									$fi['elem2'] = max($pairs);
									
									$exists = true;
								}
								elseif($subcourses_assoc_by_id[min($pairs)]['layer'] == $subcourses_assoc_by_id[max($pairs)]['layer'] && 
										$subcourses_assoc_by_id[$fi['elem1']]['layer'] == $subcourses_assoc_by_id[$fi['elem2']]['layer'] &&
										$subcourses_assoc_by_id[min($pairs)]['layer'] == $subcourses_assoc_by_id[$fi['elem1']]['layer'])
								{// если элементы в том же слое, то добавить новое пересечение фильтров (далее)						
								}
								else
								{
									$exists = true;
								}
							}
						}
					}
					unset($fi);
					
					if(!$exists)
					{// такого фильтра ещё нет, поэтому добавляем его
				
						$filter_intersections[] = [
													'elem1' => min($pairs),
													'elem2' => max($pairs),
													'intersection' => $filter_intersection
												];
					}
				}
						
				$key_for_min_elements = 0; // для сохранения минимальных элементов
				$min_subcourses = []; // минимальные элементы пересечений фильтров
				
				// 4.1 // находим минимальные элементы каждого из пересечений фильтров
				foreach($filter_intersections as $filter_inter)
				{	
					$min_subcourses[$key_for_min_elements]['elems'][] = $filter_inter['elem1'];
					$min_subcourses[$key_for_min_elements]['elems'][] = $filter_inter['elem2'];	
					$min_subcourses[$key_for_min_elements]['intersection'] = $filter_inter['intersection'];	
					$min_subcourses[$key_for_min_elements]['mins'] = [];
						
					foreach($filter_inter['intersection'] as $subcourse_id)
					{					
						$links_inside = false;
						
						// получить связи элемента пересечения 
						// ОТ других
						if(!is_null($subcourses_assoc_by_id[$subcourse_id]['links_from']))
						{
							foreach($subcourses_assoc_by_id[$subcourse_id]['links_from'] as $link_from)
							{
								if(in_array($link_from, $filter_inter['intersection'])) // если элемент-источник ВНУТРИ пересечения фильтров
								{						
									$links_inside = true; // не добавлять вдальнейшем связь
									break;
								}
							}	
						}
						
						if(!$links_inside) // элементы-источники для subcourse_id ВНЕ фильтра
						{
							$min_subcourses[$key_for_min_elements]['mins'][] = $subcourse_id; // минимальный элемент пересечения фильтров
						}
					}
					
					$key_for_min_elements++;
				}
				
				unset($layer_subcourses_pairs);
				unset($layer_subcourses_filters);
				unset($filter_intersections);
					
				// 4.2 // переходим от массива min_subcourses к min_subcourses_final: для одинаковых минимальных элементов пересечений фильтров сливаем все-все элементы-источники
				
				$min_subcourses_final = [];
				$used = []; // использованные мин. элементы пересечения фильтров, чтобы не повторяться
				
				// перебираем мин. элементы всех пересечений фильтров	
				foreach($min_subcourses as $key1 => $min_sub1)
				{
					if(count($min_sub1['mins']) > 1)
					{// оставляем только те, число мин. элементов в которых 2 и выше
				
						if(!in_array($key1, $used))
						{// если ещё не прошлись по данному набору мин. элементов
					
							// добавляем набор минимальных элементов
							$min_subcourses_final[$key1]['mins'] = $min_sub1['mins'];
							
							// добавляем элементы-источники в текущей итерации
							$min_subcourses_final[$key1]['elems'] = $min_sub1['elems'];
							
							// добавляем пересечение фильтров
							$min_subcourses_final[$key1]['intersection'] = $min_sub1['intersection'];
							
							// ищём, есть ли ещё такие же наборы мин. элементов и добавляем к уже добавленным на предыдущей строчке элементам другие элементы, из итераций цикла ниже
							foreach($min_subcourses as $key2 => $min_sub2)
							{
								if($min_sub1['mins'] == $min_sub2['mins'] && $key1 != $key2 && !in_array($key2, $used))
								{// тот же набор + НЕ тот же элемент массива (чтобы не добавить эти же элементы) + ещё не прошлись по данному набору	
							
									foreach($min_sub2['elems'] as $elem)
									{
										if(!in_array($elem, $min_subcourses_final[$key1]['elems']))
											$min_subcourses_final[$key1]['elems'][] = $elem; // добавляем элемент для набора мин. элементов
									}
									
									$used[] = $key2; // набор использован
								}
							}
						}	
					}
					
					$used[] = $key1; // набор использован
				}
				
				unset($used);
				unset($min_subcourses);
				
				unset($subcourses_assoc_by_layer[count($subcourses_assoc_by_layer)-1]); // убираем последний слой, поскольку функция отрабатывает в последний раз на ПОТОМКАХ МАКСИМАЛЬНОГО элемента решётки
				
				// 4.3 // на основе полученных минимальных элементов, если необходимо, создаём для фильтров новые минимальные элементы
				foreach($min_subcourses_final as $key_min => $subcourse)
				{
					$okey = true; // между элементами-источниками и пересечением фильтров нет других элементов
					
					foreach($subcourse['mins'] as $sub_min)
					{
						foreach($subcourse['elems'] as $selem)
						{
							if(!in_array($selem, $subcourses_assoc_by_id[$sub_min]['links_from']))
							{// между элементом selem и пересечением фильтров есть другие элементы
								$okey = false;
								break; // ломает цикл с elems
							}
						}
						
						if(!$okey)
						{				
							break;  // ломает цикл с mins
						}
						else
						{// между элементами-источниками и пересечением фильтров нет других элементов	
								
							$elems_arr = $subcourse['elems'];
							
							do 
							{
								$elems_arr_tmp = []; // новые добавленные элементы
								
								for($i=0; $i<count($elems_arr)-1; $i++)
								{													
									$pair = [
												$elems_arr[$i], 
												$elems_arr[$i+1]
											];
																	
									foreach($subcourse['mins'] as $smin)
									{				
										// перебираем связи к минимальным элементам фильтра
										foreach($subcourses_assoc_by_id[$smin]['links_from'] as $lnk)
										{
											// если связь не внутри пересечения фильтров, удалить её и сохранить для связи вдальнейшем с новым элементом
											if(!in_array($lnk, $subcourse['intersection']) && in_array($lnk, $pair))
											{
												deleteCourseLink($lnk, $smin);
											}
										}
									}
												
									$new_name = $subcourses_assoc_by_id[$pair[0]]['name'].' + '.$subcourses_assoc_by_id[$pair[1]]['name'];
									$new_layer = max($subcourses_assoc_by_id[$pair[0]]['layer'], $subcourses_assoc_by_id[$pair[1]]['layer']) + 1;

									// добавляем новый минимальный элемент фильтра
									$new_min_subcourse_id = addCourse($new_name);
									
									// делаем его подкурсом курса course_id
									addSubcourse($course_id, $new_min_subcourse_id);
											
									// добавляем (восстанавливаем старые) связи от элементов, которые были связаны с минимальными элементами фильтра, к новому минимальному элементу
									foreach($pair as $del_link) // $deletedLinks[qwe] as ... 
									{
										addCourseLink($del_link, $new_min_subcourse_id);
									}
									
									// обновляем ссылки элементов, для которых искали пересечение фильтра
									$elem1_links = getCourseLinksFromToIds(getCourseLinks($pair[0]), getCourseLinks($pair[0], true));
									$subcourses_assoc_by_id[$pair[0]]['links_from'] = $elem1_links['from'];
									$subcourses_assoc_by_id[$pair[0]]['links_to'] = $elem1_links['to'];
									
									$elem2_links = getCourseLinksFromToIds(getCourseLinks($pair[1]), getCourseLinks($pair[1], true));
									$subcourses_assoc_by_id[$pair[1]]['links_from'] = $elem2_links['from'];
									$subcourses_assoc_by_id[$pair[1]]['links_to'] = $elem2_links['to'];
									
									if(count($elems_arr) == 2)
									{
										// добавляем связи от нового к бывшим минимальным элементам
										foreach($subcourse['mins'] as $smin)
										{
											addCourseLink($new_min_subcourse_id, $smin);
											
											$min_links = getCourseLinksFromToIds(getCourseLinks($smin), getCourseLinks($smin, true));
											$subcourses_assoc_by_id[$smin]['links_from'] = $min_links['from'];
											$subcourses_assoc_by_id[$smin]['links_to'] = $min_links['to'];	
										}
									}
									
									// выясним, надо ли поднимать элементы
									$offset = 1;						
									foreach($subcourses_assoc_by_layer[$new_layer] as $subc)
									{
										if($subcourses_assoc_by_id[$subc]['added'])
										{// если в слое new_layer уже есть добавленные элементы
											$offset = 0;	
										}
									}
									
									// обновляем слои элементов в словаре элементов	
									foreach($subcourses_assoc_by_id as &$value)	
									{
										if($value['layer'] >= $new_layer)
										{
											$value['layer'] += $offset;
										}
									}
									unset($value);
									
									// добавляем новый элемент в словарь слоёв
									if($offset > 0) 
									{// если добавлен единственный элемент в слое
										array_splice($subcourses_assoc_by_layer, $new_layer, 0, array(array(intval($new_min_subcourse_id))));
									}
									else
									{// если в слое уже есть добавленные элементы
										$subcourses_assoc_by_layer[$new_layer][] = intval($new_min_subcourse_id);
									}
									
									// добавляем новый элемент в словарь элементов
									$new_links = getCourseLinksFromToIds(getCourseLinks($new_min_subcourse_id), getCourseLinks($new_min_subcourse_id, true));
									$subcourses_assoc_by_id[$new_min_subcourse_id] = ['links_from' => $new_links['from'], 'links_to' => $new_links['to'], 'name' => $new_name, 'layer' => $new_layer, 'added' => true];
									
									$elems_arr_tmp[] = $new_min_subcourse_id;
								}
								$elems_arr = $elems_arr_tmp;
							} 
							while(count($elems_arr) > 1);					
						}
					}
				}

				unset($min_subcourses_final);
					
					
				// 5 // Устанавливаем минимальный элемент решётки

				if(count($subcourses_assoc_by_layer[0]) > 1)
				{// если минимальных элементов больше 1, то добавляем для них общий минимальный элемент и назначаем каждому из них связь с добавленным элементом 
					
					// добавляем новый курс
					$new_min_subcourse_id = addCourse(NEW_MIN_SUBCOURSE_NAME);
					saveCourse($new_min_subcourse_id, NEW_MIN_SUBCOURSE_NAME.$new_min_subcourse_id);
					
					// делаем его подкурсом курса course_id
					addSubcourse($course_id, $new_min_subcourse_id);
					
					// добавляем связи от нового элемента к элементам минимального слоя
					foreach($subcourses_assoc_by_layer[0] as $min_subcourse_id)
					{
						// добавляем каждую связь в бд
						addCourseLink($new_min_subcourse_id, $min_subcourse_id);
						
						// обновляем связи
						$links = getCourseLinksFromToIds(getCourseLinks($min_subcourse_id), getCourseLinks($min_subcourse_id, true));		
						$subcourses_assoc_by_id[$min_subcourse_id]['links_from'] = $links['from'];
						$subcourses_assoc_by_id[$min_subcourse_id]['links_to'] = $links['to'];
					}
					
					foreach($subcourses_assoc_by_id as &$subc)
					{
						$subc['layer']++;
					}
					unset($subc);
					
					array_unshift($subcourses_assoc_by_layer, [intval($new_min_subcourse_id)]);
					
					// добавляем данные о новом элементе
					$new_links = getCourseLinksFromToIds(getCourseLinks($new_min_subcourse_id), getCourseLinks($new_min_subcourse_id, true));
					$subcourses_assoc_by_id[$new_min_subcourse_id] = ['links_from' => $new_links['from'], 'links_to' => $new_links['to'], 'name' => NEW_MIN_SUBCOURSE_NAME.$new_min_subcourse_id, 'layer' => 0, 'added' => true];
				}


				// 6 // Устанавливаем максимальный элемент решётки
				$last_layer = [];
				foreach($subcourses_assoc_by_id as $id => $subcourse)
				{
					if(empty($subcourse['links_to']))
					{// находим элементы без потомков

						$last_layer[] = $id;
					}	
				}

				if(count($last_layer) > 1)
				{// если последних элементов больше 1, то добавляем для них общий максимальный элемент и назначаем ему связь с каждым из них 
					
					// добавляем новый курс
					$new_max_subcourse_id = addCourse(NEW_MAX_SUBCOURSE_NAME);
					saveCourse($new_max_subcourse_id, NEW_MAX_SUBCOURSE_NAME.$new_max_subcourse_id);
					
					// делаем его подкурсом курса course_id
					addSubcourse($course_id, $new_max_subcourse_id);
					
					// добавляем связи от элементов максимального слоя к новому элементу
					foreach($last_layer as $max_subcourse_id)
					{
						// добавляем каждую связь в бд
						addCourseLink($max_subcourse_id, $new_max_subcourse_id);
						
						// обновляем связи
						$links = getCourseLinksFromToIds(getCourseLinks($max_subcourse_id), getCourseLinks($max_subcourse_id, true));		
						$subcourses_assoc_by_id[$max_subcourse_id]['links_from'] = $links['from'];
						$subcourses_assoc_by_id[$max_subcourse_id]['links_to'] = $links['to'];
					}
					
					$subcourses_assoc_by_layer[] = [intval($new_max_subcourse_id)];
							
					// добавляем данные о новом элементе
					$new_links = getCourseLinksFromToIds(getCourseLinks($new_max_subcourse_id), getCourseLinks($new_max_subcourse_id, true));
					$subcourses_assoc_by_id[$new_max_subcourse_id] = ['links_from' => $new_links['from'], 'links_to' => $new_links['to'], 'name' => NEW_MAX_SUBCOURSE_NAME.$new_max_subcourse_id, 'layer' => count($subcourses_assoc_by_layer)-1, 'added' => true];
				}	
				
				unset($last_layer);
								
				
				// 7 // Назначаем каждому элементу позицию в слое (для x-координаты)
				foreach($subcourses_assoc_by_layer as $sc_layer)
				{
					$count_in_layer = count($sc_layer);
					$coord = (-1) * X_STEP * $count_in_layer / 2;
								
					foreach($sc_layer as $sc_id)
					{
						$subcourses_assoc_by_id[$sc_id]['x'] = $coord;
						$coord += X_STEP;
					}
				}
			
			
				// 8 // Подготавливаем к рисованию 
				foreach($subcourses_assoc_by_id as $sc_id => $sc_value)
				{
					if($sc_value['added'])
						$color = NODE_COLOR_ADDED;
					else
						if(courseHasSubcourses($sc_id))
							$color = NODE_COLOR_NOTERMINAL;
						else
							$color = NODE_COLOR_TERMINAL;
						
					if($subcourses_assoc_by_id[$sc_id]['layer'] == 0)
						$url = NODE_IMAGE_MIN;
					elseif($subcourses_assoc_by_id[$sc_id]['layer'] == count($subcourses_assoc_by_layer)-1)
						$url = NODE_IMAGE_MAX;
					else
						if(courseHasContent($sc_id))
							$url = NODE_IMAGE_CONTENT;
						else
							$url = '';
					
					$x = $sc_value['x'];
					$y = (-1) * Y_STEP * $sc_value['layer'];
					
					$nodes[] = [
						'id' => $sc_id, // id элемента как курса
						'label' => $sc_value['name'], // название
						'x' => $x, 
						'y' => $y, 
						'size' => NODE_SIZE,
						'color' => $color,
						'type' => 'circle',
						'image' => ['url' => $url, 'scale '=> 1, 'clip' => 1]
					];
					
					if(!is_null($sc_value['links_from']))
					{
						// добавляем все связи, относящиеся к узлу (подкурсу)
						foreach($sc_value['links_from'] as $sc_link)
						{
							$edges[] = [
								'id' => $sc_link.'-'.$sc_id,
								'source' => $sc_link,
								'target' => $sc_id,
								'type' => EDGE_TYPE,
								'size' => EDGE_SIZE,
								'color' => EDGE_COLOR
							];
						}
					}
				}	
				
				unset($subcourses_assoc_by_id);
				unset($subcourses_assoc_by_layer);
			}
		}
		
		return ['nodes' => $nodes, 'edges' => $edges, 'errors' => $errors];
	}		
	
	/* получить курс (запись) по id */
    function getCourse($id)
	{
        global $db_con;
		$query = 'SELECT * FROM '.table('courses').' WHERE id = ?';
		$sql = $db_con->prepare($query);
		$sql->execute(array($id));
		$res = $sql->fetch(PDO::FETCH_ASSOC);
		
		return $res;
	}
	
    /* получить курс (полностью) по id */
    function getFullCourse($id)
	{
        global $db_con;
		
		if($id == 0)
		{
			$id = addCourse(NEW_COURSE_NAME);
		}
		
		$res = getCourse($id);
		
		if($res)
		{
			$res['contents'] = getCourseContent($id); // Контент			
			$res['links'] = getCourseLinks($id); // Связи					
			$res['subcourses'] = getCourseSubcourses($id); // Подкурсы и связи между ними
			if($res['subcourses'])
			{
				$graph_data = getCourseGraph($id, $res['subcourses']);
				$res['nodes'] = $graph_data['nodes']; // Узлы графа курса
				$res['edges'] = $graph_data['edges']; // Связи графа курса
			}
		}
		else
		{
			$res['id'] = -1;
		}
	
		return $res;
    }
		
	/* получить все курсы */
    function getCoursesList($name, $active, $terminal, $content)
	{
        global $db_con;		
						
		// извлечь список курсов согласно запросу: название содержит фразу + доступные/недоступные + терминальные/нетерминальные + с контентом/без контента
		$query = 'SELECT DISTINCT '.table('courses').'.id, '.table('courses').'.name, '.table('courses').'.active FROM '.table('courses').' '; 
		if($terminal >= 0) // запрос по параметру "Терминальный"
		{
			$query .=	'INNER JOIN '.table('subcourses').' ON '.table('courses').'.id '.($terminal ? 'NOT' : '').' IN (SELECT DISTINCT course_id FROM '
						.table('subcourses').') ';
		}	
		if($content >= 0) // запрос по параметру "Контент"
		{
			$sql = $db_con->prepare('SELECT DISTINCT course_id FROM '.table('courses_contents'));
			$sql->execute(array());
			$cnt = $sql->fetchAll(PDO::FETCH_ASSOC);
			
			if($cnt || $content == 1)
			{
				$query .= 'INNER JOIN '.table('courses_contents').' ON '.table('courses').'.id '.($content ? '' : 'NOT').' IN (SELECT DISTINCT course_id FROM '
						.table('courses_contents').') '; 
			}
		}
		$query .=	'WHERE '.table('courses').'.name LIKE "%'.$name.'%" ';// запрос по параметру "Название"	
		if($active >= 0)
		{
			$query .= 'AND '.table('courses').'.active = '.$active.' '; // запрос по параметру "Доступен"
		}
		$query .=	'GROUP BY '.table('courses').'.id ORDER BY '.table('courses').'.name ASC';
		
		$sql = $db_con->prepare($query);
        $sql->execute(array());
        $res = $sql->fetchAll(PDO::FETCH_ASSOC);
				
		foreach($res as &$r)
		{
			if($terminal >= 0) // есть запрос по параметру "Терминальный"
				$r['terminal'] = $terminal ? true : false;
			else
				$r['terminal'] = !courseHasSubcourses($r['id']);
			
			if($content >= 0) // есть запрос по параметру "Контент"
				$r['has_content'] = $content ? true : false;
			else
				$r['has_content'] = courseHasContent($r['id']);
		}
		unset($r);
				
		return $res;
    }	
		
	/* добавить курс */
    function addCourse($name, $description = null, $active = 1)
	{
        global $db_con;
		
		if($name != NEW_COURSE_NAME)
		{
			$check_query = 'SELECT COUNT(id) AS cnt FROM '.table('courses').' WHERE name = ?';
			$check_sql = $db_con->prepare($check_query);
			$check_sql->execute(array($name));
			$check_res = $check_sql->fetch(PDO::FETCH_ASSOC);
			
			if($check_res['cnt'] > 0) // если курс с таким названием уже существует, приписываем к названию постфикс с номером
			   $name .= '_'.($check_res['cnt'] + 1);
		}	   
	   	
		$query = 'INSERT INTO '.table('courses').'(name,description,active) VALUES(?,?,?)';
		$sql = $db_con->prepare($query);
		$sql->execute(array($name, $description, $active));
		$course_id = $db_con->lastInsertId();
		
		return $course_id;
    }
		
	/* сохранить курс */
    function saveCourse($id, $name = null, $description = null, $active = null)
	{		
        global $db_con;
		
		$set = [];		
		if(!is_null($name))
		{
			$set[] = 'name = ?';
			$params[] = $name;
		}
		
		if(!is_null($description))
		{
			$set[] = 'description = ?';
			$params[] = $description;
		}
		
		if(!is_null($active))
		{
			$set[] = 'active = ?';
			$params[] = $active;
	   	}
		
		$params[] = $id;	
		
		$query = 'UPDATE '.table('courses').(empty($set) ? '' : ' SET '.implode(',',$set)).' WHERE id = ?';
		$sql = $db_con->prepare($query);
		$sql->execute($params);
    }
	
	/* получить подкурс (соединение с таблицей курсов для получения названия каждого из них) */
	function getSubcourse($subcourse_rec_id)
	{
        global $db_con;
		$query = 'SELECT * FROM '.table('subcourses').' INNER JOIN '.table('courses').' ON '.table('subcourses').'.subcourse_id = '.table('courses').'.id AND '.table('subcourses').'.id = ? AND '.table('courses').'.active = 1';
		$sql = $db_con->prepare($query);
		$sql->execute(array($subcourse_rec_id));
		$subcourse = $sql->fetch(PDO::FETCH_NAMED);
		
		return $subcourse;
	}
	
	/* добавить подкурс */
	function addSubcourse($course_id, $subcourse_id)
	{
        global $db_con;
		$query = 'INSERT INTO '.table('subcourses').'(course_id,subcourse_id) VALUES(?,?)';;
		$sql = $db_con->prepare($query);
		$sql->execute(array($course_id, $subcourse_id));
        $subcourse_rec_id = $db_con->lastInsertId();
		
		return $subcourse_rec_id;
	}
		
	/* удалить подкурс */
	function deleteSubcourse($subcourse_id)
	{
        global $db_con;
		$query = 'DELETE FROM '.table('subcourses').' WHERE id = ?';
		$sql = $db_con->prepare($query);
		$sql->execute(array($subcourse_id));
	}
	
	
	/* привести структуру курса к алгебраической решётке */
	function courseToLattice($course_id)
	{				
		$subcourses = getCourseSubcourses($course_id); // Подкурсы и связи между ними
		if($subcourses)
		{			
			$graph = getCourseGraph($course_id, $subcourses); // узлы и связи графа курса
		}
		
		return $graph;
	}
?>