<?php

if(!isset($_POST['allow'])) { header('Location: /403'); } /* Против вызова напрямую из браузера */

include '../config.php';
include '../functions.php';

$course_id = $_POST['course_id'];

dbconnect();
$graph = courseToLattice($course_id); // приведение структуры курса к алгебраической решётке
dbclose();

echo json_encode(array('nodes' => json_encode($graph['nodes']), 'edges' => json_encode($graph['edges']), 'errors' => json_encode($graph['errors'])));

?>