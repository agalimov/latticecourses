<?php

if(!isset($_POST['allow'])) { header('Location: /403'); } /* Против вызова напрямую из браузера */

include '../config.php';
include '../functions.php';

$course_id = $_POST['course_id'];

dbconnect();
$course = getFullCourse($course_id); // получить всю информацию о курсе
dbclose();

ob_start();
if($course['subcourses'])
{
	foreach($course['subcourses'] as $subcourse)
	{
		if($subcourse['links']) 
		{
			$hide = false; // не прятать, так как не ajax, не нужен анимированный показ
			foreach($subcourse['links'] as $link) 
			{		
				include '/get-course-link/course-link.php'; // вёрстка связи, здесь используется $link
				$links_html_arr[$subcourse['id'][0]] = ob_get_contents(); // массив вёрсток, для обращения в следующем цикле
			}	
			ob_clean();	
		}
	}				
	
	foreach($course['subcourses'] as $subcourse)
	{
		$links_html = $links_html_arr[$subcourse['id'][0]];
		include '/get-subcourse/subcourse.php'; // вёрстка подкурса, здесь используется $links_html
		$subcourses_html = ob_get_contents();
	}
	ob_clean();
}

include '/get-course/course-tab.php'; 
$tab = ob_get_contents(); 
ob_clean();

include '/get-course/course-div.php'; // здесь используется $subcourses_html
$div = ob_get_contents();
ob_end_clean();

echo json_encode(array('course_id' => $course['id'], 'tab' => $tab, 'div' => $div, 'nodes' => json_encode($course['nodes']), 'edges' => json_encode($course['edges'])));

?>