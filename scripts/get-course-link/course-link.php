<div data-link-id="<?= $link['id'][0] ?>" data-source-id="<?= $link['src_id'] ?>" class="link-item nonew" <?= $hide ? 'style="display:none"' : '' ?> title="<?= $link['name'] ?>">
	<div class="link-name"><?= $link['name'] ?></div>
	<a title="Удалить связь" class="delete-link pull-right"><span class="glyphicon glyphicon-remove"></span></a>
</div>