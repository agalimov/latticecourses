<?php

if(!isset($_POST['allow'])) { header('Location: /403'); } /* Против вызова напрямую из браузера */

include '../config.php';
include '../functions.php';

$link_id = $_POST['link_id'];

dbconnect();
deleteCourseLinkById($link_id); // удаляем связь по id
dbclose();

?>