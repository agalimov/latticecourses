<?php

if(!isset($_POST['allow'])) { header('Location: /403'); } /* Против вызова напрямую из браузера */

include '../config.php';
include '../functions.php';

$course_id = $_POST['course_id'];
$course_name = $_POST['course_name'];

dbconnect();

$subcourse_id = addCourse($course_name); // добавить курс
$subcourse_record_id = addSubcourse($course_id, $subcourse_id); // сделать его подкурсом
$subcourse = getSubcourse($subcourse_record_id); // и получить информацию о подкурсе

$course['subcourses'] = getCourseSubcourses($subcourse['course_id']); // получить другие подкурсы (для заполнения списка для создания связей)

dbclose();

ob_start();

include '/get-subcourse/subcourse.php'; // вёрстка подкурса
$subcourse_html = ob_get_contents();

ob_end_clean();

echo json_encode(array('subcourse' => $subcourse_html, 'subcourse_id' => $subcourse['id'][1] /* id подкурса как курса */, 'subcourse_name' => $subcourse['name']));

?>