<?php if(!isset($_POST['allow'])) { header('Location: /403'); } /* Против вызова напрямую из браузера */ ?>
<tr class="get-course <?= $course['active'] ? 'c-active' : 'c-noactive' ?>" data-dismiss="modal" course-id="<?= $course['id'] ?>">
	<td>
		<?= $course['name'] ?>
		<div class="pull-right <?= $course['terminal'] ? 'terminal' : 'noterminal' ?>"></div>
		<div class="pull-right"><?= $course['has_content'] ? '<span class="glyphicon glyphicon-paperclip"></span>' : '' ?></div>
	</td>
</tr>