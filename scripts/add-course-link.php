<?php

if(!isset($_POST['allow'])) { header('Location: /403'); } /* Против вызова напрямую из браузера */

include '../config.php';
include '../functions.php';

$source_id = $_POST['source_id'];
$destination_id = $_POST['destination_id'];

dbconnect();
$link_id = addCourseLink($source_id, $destination_id); // добавляем новую связь ...
$link = getCourseLink($link_id); // ... и получаем информацию о ней
dbclose();

ob_start();

$hide = true;
include '/get-course-link/course-link.php'; // вёрстка связи
$course_link_html = ob_get_contents();

ob_end_clean();

echo json_encode(array('course_link' => $course_link_html));

?>