<?php

if(!isset($_POST['allow'])) { header('Location: /403'); } /* Против вызова напрямую из браузера */

include '../config.php';
include '../functions.php';

dbconnect();
$courses = getCoursesList($_POST['q_name'], $_POST['q_active'], $_POST['q_terminal'], $_POST['q_content']); // поиск курсов по запросу
dbclose();

ob_start();

$courses_table = '';

if($courses)
{
	foreach($courses as $course)
	{		
		include '/get-courses-list/course-list-item.php'; // вёрстка строки курса в окне поиска, здесь используется $course
		$courses_table = ob_get_contents();
	}
}
ob_end_clean();

echo json_encode(array('table' => $courses_table, 'cnt' => 'Найдено: '.count($courses)));

?>