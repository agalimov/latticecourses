<?php

if(!isset($_POST['allow'])) { header('Location: /403'); } /* Против вызова напрямую из браузера */

include '../config.php';
include '../functions.php';

$link_ids = json_decode($_POST['link_ids']);

dbconnect();
foreach($link_ids as $l_id)
{
	deleteCourseLinkById($l_id); // удаляем связь по id
}
dbclose();

?>