<?php if(!isset($_POST['allow'])) { header('Location: /403'); } /* Против вызова напрямую из браузера */ ?>
<tr data-subcourse-id="<?= $subcourse['subcourse_id'] ?>" data-record-subcourse-id="<?= $subcourse['id'][1] ?>" class="subcourse <?= !isset($subcourse) ? 'new' : 'nonew' ?>">
	<?php /* $subcourse['id'][0] : [0] - id подкурса как курса (запись в таблице courses), [1] - id подкурса как записи таблицы subcourses */ ?>
	<td class="subcourses-info">		
		<div <?= !isset($subcourse) ? 'style="display:none"' : '' ?> class="row">
			<div class="links-control">
				<div class="col-md-6">
					<?php if(isset($subcourse)) { ?>
						<div class="form-control subcourse-name get-course" title="<?= $subcourse['name'] ?>">
							<div class="text-name"><?= $subcourse['name'] ?></div>
						</div>				
					<?php } else { ?>
						<input class="form-control subcourse-name" type="text" maxlength="256" placeholder="* Название подкурса" name="i_subcourse_name"/>	
					<?php } ?>
				</div>
				<div class="col-md-4">
					<input <?= !isset($subcourse) ? 'style="display:none"' : '' ?> list="subcourse-link-<?= !isset($subcourse) ? 'new' : $subcourse['id'][0] ?>" class="form-control subcourse-link pull-left" name="i_subcourse_link"/>
					<datalist id="subcourse-link-<?= !isset($subcourse) ? 'new' : $subcourse['id'][0] ?>">
						<?php if(isset($subcourse)) { ?>
							<?php foreach($course['subcourses'] as $sc) { ?>
								<?php if($sc['id'][0] != $subcourse['id'][1]) { ?>	
									<option value="<?= $sc['name'] ?>" data-source-id="<?= $sc['subcourse_id'] ?>"/>
								<?php } ?>
							<?php } ?>
						<?php } ?>
					</datalist>
				</div>
				<div class="col-md-2 subcourse-delclear">
					<div class="pull-right">
						<button title="Очистить связи" <?= (!is_null($subcourse['links']) && !empty($subcourse['links'])) ? '' : 'style="display:none"' ?> class="btn btn-sm btn-danger clear-subcourse"><span class="glyphicon glyphicon-erase"></span></button>
						<button title="Удалить подкурс" class="btn btn-sm btn-danger delete-subcourse"><span class="glyphicon glyphicon-remove"></span></button>
					</div>
				</div>
			</div>
			<div class="col-md-12">		
				<div class="links-list">
					<?= $links_html ?>
				</div>
			</div>
		</div>	
	</td>
</tr>