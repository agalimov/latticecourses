<?php

if(!isset($_POST['allow'])) { header('Location: /403'); } /* Против вызова напрямую из браузера */

include '../config.php';
include '../functions.php';

$course_id = $_POST['course_id'];
$course_name = $_POST['course_name'];

dbconnect();

$subcourse_id = saveCourse($course_name);
$subcourse_record_id = addSubcourse($course_id, $subcourse_id);
$subcourse = getSubcourse($subcourse_record_id);

$course['subcourses'] = getCourseSubcourses($subcourse['course_id']);

dbclose();

ob_start();

include '/get-subcourse/subcourse.php'; 
$subcourse_html = ob_get_contents();

ob_end_clean();

echo json_encode(array('subcourse' => $subcourse_html, 'subcourse_record_id' => $subcourse['id'][0], 'subcourse_name' => $subcourse['name']));

?>