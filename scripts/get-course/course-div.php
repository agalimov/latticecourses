<?php if(!isset($_POST['allow'])) { header('Location: /403'); } /* Против вызова напрямую из браузера */ ?>
<div data-course-id="<?= $course['id'] ?>" class="course-div tab-pane" id="course-div-<?= $course['id'] ?>">

	<!-- Управление курсом -->
	<div class="course-management-wrap col-md-7 col-lg-6">							
		<div class="course-management">			
			
			<!-- Режимы редактирования -->
			
			<div class="row">
				<div class="col-md-9">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#course-main-<?= $course['id'] ?>" data-toggle="tab">Основное</a></li>
						<li><a href="#course-content-<?= $course['id'] ?>" data-toggle="tab">Контент&nbsp;<span class="badge content-count"><?= count($course['contents']) ?></span></a></li>		
						<li><a href="#course-structure-<?= $course['id'] ?>" data-toggle="tab">Структура&nbsp;<span class="badge subc-count"><?= count($course['subcourses']) ?></span></a></li>	
						<li><a href="#course-links-<?= $course['id'] ?>" data-toggle="tab">Связи</a></li>	
						<li><a href="#course-operations-<?= $course['id'] ?>" data-toggle="tab">Операции</a></li>							
					</ul>	
				</div>	
				<div class="col-md-3">
					<div class="pull-right">
						<button title="Обновить" class="btn btn-warning course-refresh"><span class="glyphicon glyphicon-refresh"></span></button>
						<!-- <button title="Сохранить" class="btn btn-primary course-save"><span class="glyphicon glyphicon-floppy-disk"></span></button> -->
					</div>
				</div>
			</div>
			
			<!-- Редактирование в выбранном режиме -->
			<div class="tab-content">	
			
				<!-- Основное -->
				<div class="tab-pane course-main active" id="course-main-<?= $course['id'] ?>">	
					<div class="row-main row">
						<div class="col-md-8">
							<label for="course-name-<?= $course['id'] ?>">Название *</label>
							<input type="text" maxlength="256" id="course-name-<?= $course['id'] ?>" class="form-control" name="i_course_name" value="<?= $course['name'] ?>"/>
						</div>
						<div class="col-md-3">
							<label for="course-active-<?= $course['id'] ?>">Доступен</label>
							<select id="course-active-<?= $course['id'] ?>" class="form-control course-active">
								<option value="1" <?= $course['active'] ? 'selected' : '' ?>>Да</option>								
								<option value="0" <?= $course['active'] ? '' : 'selected' ?>>Нет</option>								
							</select>
						</div>
						<div class="col-md-1">
							<div class="pull-right">
								<button title="Сохранить" class="btn btn-primary course-save"><span class="glyphicon glyphicon-floppy-disk"></span></button>
							</div>
						</div>
						<div class="col-course-description col-md-12">
							<label for="course-description-<?= $course['id'] ?>">Описание</label>
							<textarea class="form-control" id="course-description-<?= $course['id'] ?>" name="i_course_description"><?= $course['description'] ?></textarea>
						</div>
					</div>
				</div>
				
				<!-- Контент -->
				<div class="tab-pane course-content" id="course-content-<?= $course['id'] ?>">
					Контент:
					</br>
					Список связанных файлов и загрузчик
				</div>
				
				<!-- Структура -->
				<div class="tab-pane course-structure" id="course-structure-<?= $course['id'] ?>">				
					<button title="Добавить подкурс" class="btn btn-success add-subcourse"><span class="glyphicon glyphicon-plus"></span></button>

					<div class="pull-right">
						<button title="Очистить структуру" class="btn btn-danger clear-structure"><span class="glyphicon glyphicon-erase"></span></button>
						<button title="Достроить до решётки" class="btn btn-primary course-to-lattice"><span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;<span class="glyphicon glyphicon-arrow-right"></span>&nbsp;&nbsp;<span class="glyphicon glyphicon-th"></button>
					</div>			
					
					<div class="table-wrap-border">
						<div class="table-wrap">
							<table class="table table-bordered">
								<tbody>
									<?= $subcourses_html ?>
								</tbody>
							</table>
						</div>	
					</div>	
				</div>		
				
				<!-- Связи -->
				<div class="tab-pane course-links" id="course-links-<?= $course['id'] ?>">
					Связи:
					</br>
					Какие курсы включают в себя этот курс
				</div>
							
				<!-- Операции -->
				<div class="tab-pane course-operations" id="course-operations-<?= $course['id'] ?>">
					<div class="row">
						<div class="col-md-10">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#course-extension-<?= $course['id'] ?>" data-toggle="tab">Расширение</a></li>	
								<li><a href="#course-fragmentation-<?= $course['id'] ?>" data-toggle="tab">Фрагментирование</a></li>					
							</ul>
						</div>
						<div class="col-md-2">	
							<div class="pull-right">
								<div class="btn-group course-delclear">
									<button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-trash"></span></button>
									<ul class="dropdown-menu">
										<li class="course-clear"><a href="#">Очистить</a></li>
										<li class="course-delete"><a href="#">Удалить</a></li>
									</ul>
								</div>								
							</div>
						</div>
					</div>				
					
					<div class="tab-content">
						
						<!-- Расширение -->
						<div class="tab-pane course-extension active" id="course-extension-<?= $course['id'] ?>">
							Расширение:
							</br>
							Выбрать курсы, для которых данный курс будет подкурсом,
							</br>
							и указать, в каких из них произвести декомпозицию данного курса
						</div>
						
						<!-- Фрагментирование -->
						<div class="tab-pane course-fragmentation" id="course-fragmentation-<?= $course['id'] ?>">
							Фрагментирование:
							</br>
							Выбор некоторых узлов, с которыми курс имеет связи,
							</br>
							и объединение в новый курс
						</div>		
						
					</div>					
				</div>	
				
			</div>
			
		</div>
	</div>
	
	<!-- Граф курса -->
	<div unselectable="on" class="course-graph-wrap col-md-5 col-lg-6">
		<div class="course-graph" id="course-graph-<?= $course['id'] ?>">
		</div>
	</div>
	
</div>