<?php

include 'config.php';
include 'functions.php';
		
dbconnect();

$course_id = 8;

$nodes = []; // узлы
$edges = []; // и ребра графа

$subcourses_assoc_by_id = []; // ключ - id подкурса, значение - массив: [массив связей К от других, массив связей ОТ к другим, название, номер слоя]
$subcourses_assoc_by_layer = []; // ключ - номер слоя, значение - массив id`s элементов
$errors = []; // ошибки структуры курса

$subcourses = getCourseSubcourses($course_id);

foreach($subcourses as &$subcourse)
{
	$subcourse['links_to'] = getCourseLinks($subcourse['id'][0], true); // получить связи К элементам-потомкам			
	$links = getCourseLinksFromToIds($subcourse['links'], $subcourse['links_to']); // получить связи в виде двух массивов с id`s элементов
	
	if(empty($links['from']) && empty($links['to']))
	{// элемент не связн никакими связями
		$errors[] = 'Элемент '.$subcourse['name'].' не имеет связей!';
	}
	else
	{
		if(empty($errors))
		{// если ошибок нет, продолжать формировать массив
			$subcourses_assoc_by_id[$subcourse['id'][0]] = ['links_from' => $links['from'], 'links_to' => $links['to'], 'name' => $subcourse['name'], 'layer' => -1, 'added' => false];
		}
	}
}
unset($subcourse);
unset($links);
unset($subcourses);

if(empty($errors))
{// связи есть у всех элементов
	
	$min_layer = 0; // слой, с которого начинается 3-й этап 

	// 1 // Получаем минимальные узлы (не имеющие связей от других узлов) 

	foreach($subcourses_assoc_by_id as $id => &$subcourse)
	{
		if(count($subcourse['links_from']) == 0)
		{// выбираем элементы без предков

			$subcourse['layer'] = 0; 								 // назначаем первый слой
			$subcourses_assoc_by_layer[$subcourse['layer']][] = $id; // добавляем элемент в первый слой
		}
		else { break; }	// как только элементы без предков заканчиваются, идём дальше
	}
	unset($subcourse);

	
	// 4 // Назначаем каждому элементу слой (для y-координаты)

	/// Работа начинается с минимального слоя. После расчёта фильтров всех его элементов становятся известны все слои. Для элементов этих (всех остальных) слоёв операция расчёта фильтров выполняется отдельно 
	// получаем элементы минимального слоя 

	// подготавливаем полученные элементы 
	if(empty($subcourses_assoc_by_layer[0]))
	{
		$errors[] = 'В структуре отсутствуют минимальные элементы!';
	}
	else
	{
		$layer_subcourses_filters = []; // массив главных фильтров элементов слоя (id`s ВСЕХ потомков)
		foreach($subcourses_assoc_by_layer[0] as $min_subcourse_id)
		{
			// ищем ФИЛЬТРЫ для элементов МИНИМАЛЬНОГО СЛОЯ и обновляем НОМЕРА СЛОЁВ ВСЕХ элементов (subcourses_assoc_by_id) и ЭЛЕМЕНТОВ В этих СЛОЯХ (subcourses_assoc_by_layer)
			getSubcoursesFilters($layer_subcourses_filters[$min_subcourse_id], $subcourses_assoc_by_id[$min_subcourse_id]['links_to'], $subcourses_assoc_by_id, $subcourses_assoc_by_layer, 1, true, $errors);	
			
			/*if(!empty($errors))
				break;*/		
		}
	}

	// здесь необходимо убирать последний слой, но это делается далее перед шагом 5.3, поскольку здесь по какой-то причине не отрабатывает
	
	var_dump($errors);
		
	if(empty($errors))
	{// минимальные элементы присутсвуют

		/// Расчёт фильтров всех остальных элементов
		foreach($subcourses_assoc_by_layer as $num_layer => $layer_subcourses)
		{
			if($num_layer > 0)
			{			
				// фильтры
				foreach($layer_subcourses as $ls)
				{
					getSubcoursesFilters($layer_subcourses_filters[$ls], $subcourses_assoc_by_id[$ls]['links_to'], $subcourses_assoc_by_id, $subcourses_assoc_by_layer, $num_layer+1, false, $errors);
				}
			}			
		}

		unset($min_layer);
		unset($min_layer_subcourses);
		
			
		// составляем всевозможные пары элементов, без указаний на самих себя и повторений (т.е. не 1-2, 2-1, 1-1, 2-2, а только 1-2)
		/*$layer_subcourses_pairs = []; // массив пар элементов (id`s)
		foreach($subcourses_assoc_by_id as $k1=>$ls1)
		{
			foreach($subcourses_assoc_by_id as $k2=>$ls2)
			{
				if($k1 != $k2 && !in_array([$k2, $k1], $layer_subcourses_pairs))
				{
					$layer_subcourses_pairs[] = [$k1, $k2];
				}
			}
		}
	
		// 5 // Выполняем для каждой пары элементов (в одном слое) следующие действия

		// Найдём всевозможные пересечения фильтров БЕЗ ПОВТОРЕНИЙ
		$filter_intersections = [];
		foreach($layer_subcourses_pairs as $pairs)
		{
			// находим пересечение фильтров элементов pairs[0] и pairs[1]
			$filter_intersection = array_intersect($layer_subcourses_filters[$pairs[0]], $layer_subcourses_filters[$pairs[1]]);
			
			$exists = false;
			
			// проверяем, добавлялось ли уже такое пересечение фильтров
			foreach($filter_intersections as &$fi)
			{
				if(empty(array_diff($filter_intersection, $fi['intersection'])) && empty(array_diff($fi['intersection'], $filter_intersection)))
				{// если такой фильтр уже есть
								
					if(min($pairs) != $fi['elem1'] || max($pairs) != $fi['elem2'])
					{// но элементы-источники пересечения фильтра другие
						
						if(min($subcourses_assoc_by_id[min($pairs)]['layer'], $subcourses_assoc_by_id[max($pairs)]['layer']) 
							> 
						   min($subcourses_assoc_by_id[$fi['elem1']]['layer'], $subcourses_assoc_by_id[$fi['elem2']]['layer']))
						{// сохранить для фильтра элементы-источники с наибольшим (наивысшим) слоем
						
							$fi['elem1'] = min($pairs);
							$fi['elem2'] = max($pairs);
							
							$exists = true;
						}
						elseif($subcourses_assoc_by_id[min($pairs)]['layer'] == $subcourses_assoc_by_id[max($pairs)]['layer'] && 
								$subcourses_assoc_by_id[$fi['elem1']]['layer'] == $subcourses_assoc_by_id[$fi['elem2']]['layer'] &&
								$subcourses_assoc_by_id[min($pairs)]['layer'] == $subcourses_assoc_by_id[$fi['elem1']]['layer'])
						{// если элементы в том же слое, то добавить новое пересечение фильтров (далее)						
						}
						else
						{
							$exists = true;
						}
					}
				}
			}
			unset($fi);
			
			if(!$exists)
			{// такого фильтра ещё нет, поэтому добавляем его
		
				$filter_intersections[] = [
											'elem1' => min($pairs),
											'elem2' => max($pairs),
											'intersection' => $filter_intersection
										];
			}
		}
				
		$key_for_min_elements = 0; // для сохранения минимальных элементов
		$min_subcourses = []; // минимальные элементы пересечений фильтров
		
		// 5.1 // находим минимальные элементы каждого из пересечений фильтров
		foreach($filter_intersections as $filter_inter)
		{	
			$min_subcourses[$key_for_min_elements]['elems'][] = $filter_inter['elem1'];
			$min_subcourses[$key_for_min_elements]['elems'][] = $filter_inter['elem2'];	
			$min_subcourses[$key_for_min_elements]['intersection'] = $filter_inter['intersection'];	
			$min_subcourses[$key_for_min_elements]['mins'] = [];
				
			foreach($filter_inter['intersection'] as $subcourse_id)
			{					
				$links_inside = false;
				
				// получить связи элемента пересечения 
				// ОТ других
				if(!is_null($subcourses_assoc_by_id[$subcourse_id]['links_from']))
				{
					foreach($subcourses_assoc_by_id[$subcourse_id]['links_from'] as $link_from)
					{
						if(in_array($link_from, $filter_inter['intersection'])) // если элемент-источник ВНУТРИ пересечения фильтров
						{						
							$links_inside = true; // не добавлять вдальнейшем связь
							break;
						}
					}	
				}
				
				if(!$links_inside) // элементы-источники для subcourse_id ВНЕ фильтра
				{
					$min_subcourses[$key_for_min_elements]['mins'][] = $subcourse_id; // минимальный элемент пересечения фильтров
				}
			}
			
			$key_for_min_elements++;
		}
		
		unset($layer_subcourses_pairs);
		unset($layer_subcourses_filters);
		unset($filter_intersections);
			
		// 5.2 // переходим от массива min_subcourses к min_subcourses_final: для одинаковых минимальных элементов пересечений фильтров сливаем все-все элементы-источники
		
		$min_subcourses_final = [];
		$used = []; // использованные мин. элементы пересечения фильтров, чтобы не повторяться
		
		// перебираем мин. элементы всех пересечений фильтров	
		foreach($min_subcourses as $key1 => $min_sub1)
		{
			if(count($min_sub1['mins']) > 1)
			{// оставляем только те, число мин. элементов в которых 2 и выше
		
				if(!in_array($key1, $used))
				{// если ещё не прошлись по данному набору мин. элементов
			
					// добавляем набор минимальных элементов
					$min_subcourses_final[$key1]['mins'] = $min_sub1['mins'];
					
					// добавляем элементы-источники в текущей итерации
					$min_subcourses_final[$key1]['elems'] = $min_sub1['elems'];
					
					// добавляем пересечение фильтров
					$min_subcourses_final[$key1]['intersection'] = $min_sub1['intersection'];
					
					// ищём, есть ли ещё такие же наборы мин. элементов и добавляем к уже добавленным на предыдущей строчке элементам другие элементы, из итераций цикла ниже
					foreach($min_subcourses as $key2 => $min_sub2)
					{
						if($min_sub1['mins'] == $min_sub2['mins'] && $key1 != $key2 && !in_array($key2, $used))
						{// тот же набор + НЕ тот же элемент массива (чтобы не добавить эти же элементы) + ещё не прошлись по данному набору	
					
							foreach($min_sub2['elems'] as $elem)
							{
								if(!in_array($elem, $min_subcourses_final[$key1]['elems']))
									$min_subcourses_final[$key1]['elems'][] = $elem; // добавляем элемент для набора мин. элементов
							}
							
							$used[] = $key2; // набор использован
						}
					}
				}	
			}
			
			$used[] = $key1; // набор использован
		}
		
		unset($used);
		unset($min_subcourses);
		
		unset($subcourses_assoc_by_layer[count($subcourses_assoc_by_layer)-1]); // убираем последний слой, поскольку функция отрабатывает в последний раз на ПОТОМКАХ МАКСИМАЛЬНОГО элемента решётки
		
		// 5.3 // на основе полученных минимальных элементов, если необходимо, создаём для фильтров новые минимальные элементы
		foreach($min_subcourses_final as $key_min => $subcourse)
		{
			$okey = true; // между элементами-источниками и пересечением фильтров нет других элементов
			
			foreach($subcourse['mins'] as $sub_min)
			{
				foreach($subcourse['elems'] as $selem)
				{
					if(!in_array($selem, $subcourses_assoc_by_id[$sub_min]['links_from']))
					{// между элементом selem и пересечением фильтров есть другие элементы
						$okey = false;
						break; // ломает цикл с elems
					}
				}
				
				if(!$okey)
				{				
					break;  // ломает цикл с mins
				}
				else
				{// между элементами-источниками и пересечением фильтров нет других элементов	
						
					$elems_arr = $subcourse['elems'];
					
					do 
					{
						$elems_arr_tmp = []; // новые добавленные элементы
						
						for($i=0; $i<count($elems_arr)-1; $i++)
						{													
							$pair = [
										$elems_arr[$i], 
										$elems_arr[$i+1]
									];
															
							foreach($subcourse['mins'] as $smin)
							{				
								// перебираем связи к минимальным элементам фильтра
								foreach($subcourses_assoc_by_id[$smin]['links_from'] as $lnk)
								{
									// если связь не внутри пересечения фильтров, удалить её и сохранить для связи вдальнейшем с новым элементом
									if(!in_array($lnk, $subcourse['intersection']) && in_array($lnk, $pair))
									{
										deleteCourseLink($lnk, $smin);
									}
								}
							}
										
							$new_name = $subcourses_assoc_by_id[$pair[0]]['name'].' + '.$subcourses_assoc_by_id[$pair[1]]['name'];
							$new_layer = max($subcourses_assoc_by_id[$pair[0]]['layer'], $subcourses_assoc_by_id[$pair[1]]['layer']) + 1;

							// добавляем новый минимальный элемент фильтра
							$new_min_subcourse_id = addCourse($new_name);
							
							// делаем его подкурсом курса course_id
							addSubcourse($course_id, $new_min_subcourse_id);
									
							// добавляем (восстанавливаем старые) связи от элементов, которые были связаны с минимальными элементами фильтра, к новому минимальному элементу
							foreach($pair as $del_link) // $deletedLinks[qwe] as ... 
							{
								addCourseLink($del_link, $new_min_subcourse_id);
							}
							
							// обновляем ссылки элементов, для которых искали пересечение фильтра
							$elem1_links = getCourseLinksFromToIds(getCourseLinks($pair[0]), getCourseLinks($pair[0], true));
							$subcourses_assoc_by_id[$pair[0]]['links_from'] = $elem1_links['from'];
							$subcourses_assoc_by_id[$pair[0]]['links_to'] = $elem1_links['to'];
							
							$elem2_links = getCourseLinksFromToIds(getCourseLinks($pair[1]), getCourseLinks($pair[1], true));
							$subcourses_assoc_by_id[$pair[1]]['links_from'] = $elem2_links['from'];
							$subcourses_assoc_by_id[$pair[1]]['links_to'] = $elem2_links['to'];
							
							if(count($elems_arr) == 2)
							{
								// добавляем связи от нового к бывшим минимальным элементам
								foreach($subcourse['mins'] as $smin)
								{
									addCourseLink($new_min_subcourse_id, $smin);
									
									$min_links = getCourseLinksFromToIds(getCourseLinks($smin), getCourseLinks($smin, true));
									$subcourses_assoc_by_id[$smin]['links_from'] = $min_links['from'];
									$subcourses_assoc_by_id[$smin]['links_to'] = $min_links['to'];	
								}
							}
							
							// выясним, надо ли поднимать элементы
							$offset = 1;						
							foreach($subcourses_assoc_by_layer[$new_layer] as $subc)
							{
								if($subcourses_assoc_by_id[$subc]['added'])
								{// если в слое new_layer уже есть добавленные элементы
									$offset = 0;	
								}
							}
							
							// обновляем слои элементов в словаре элементов	
							foreach($subcourses_assoc_by_id as &$value)	
							{
								if($value['layer'] >= $new_layer)
								{
									$value['layer'] += $offset;
								}
							}
							unset($value);
							
							// добавляем новый элемент в словарь слоёв
							if($offset > 0) 
							{// если добавлен единственный элемент в слое
								array_splice($subcourses_assoc_by_layer, $new_layer, 0, array(array(intval($new_min_subcourse_id))));
							}
							else
							{// если в слое уже есть добавленные элементы
								$subcourses_assoc_by_layer[$new_layer][] = intval($new_min_subcourse_id);
							}
							
							// добавляем новый элемент в словарь элементов
							$new_links = getCourseLinksFromToIds(getCourseLinks($new_min_subcourse_id), getCourseLinks($new_min_subcourse_id, true));
							$subcourses_assoc_by_id[$new_min_subcourse_id] = ['links_from' => $new_links['from'], 'links_to' => $new_links['to'], 'name' => $new_name, 'layer' => $new_layer, 'added' => true];
							
							$elems_arr_tmp[] = $new_min_subcourse_id;
						}
						$elems_arr = $elems_arr_tmp;
					} 
					while(count($elems_arr) > 1);					
				}
			}
		}

		unset($min_subcourses_final);
		
		
		// 6 // Назначаем каждому элементу позицию в слое (для x-координаты)
		foreach($subcourses_assoc_by_layer as $sc_layer)
		{
			$count_in_layer = count($sc_layer);
			$coord = (-1) * X_STEP * $count_in_layer / 2;
						
			foreach($sc_layer as $sc_id)
			{
				$subcourses_assoc_by_id[$sc_id]['x'] = $coord;
				$coord += X_STEP;
			}
		}
	
	
		// 7 // Подготавливаем к рисованию 
		foreach($subcourses_assoc_by_id as $sc_id => $sc_value)
		{
			if($sc_value['added'])
				$color = NODE_COLOR_ADDED;
			else
				if(courseHasSubcourses($sc_id))
					$color = NODE_COLOR_NOTERMINAL;
				else
					$color = NODE_COLOR_TERMINAL;
				
			if($subcourses_assoc_by_id[$sc_id]['layer'] == 0)
				$url = NODE_IMAGE_MIN;
			elseif($subcourses_assoc_by_id[$sc_id]['layer'] == count($subcourses_assoc_by_layer)-1)
				$url = NODE_IMAGE_MAX;
			else
				if(courseHasContent($sc_id))
					$url = NODE_IMAGE_CONTENT;
				else
					$url = '';
			
			$x = $sc_value['x'];
			$y = (-1) * Y_STEP * $sc_value['layer'];
			
			$nodes[] = [
				'id' => $sc_id, // id элемента как курса
				'label' => $sc_value['name'], // название
				'x' => $x, 
				'y' => $y, 
				'size' => NODE_SIZE,
				'color' => $color,
				'type' => 'circle',
				'image' => ['url' => $url, 'scale '=> 1, 'clip' => 1]
			];
			
			if(!is_null($sc_value['links_from']))
			{
				// добавляем все связи, относящиеся к узлу (подкурсу)
				foreach($sc_value['links_from'] as $sc_link)
				{
					$edges[] = [
						'id' => $sc_link.'-'.$sc_id,
						'source' => $sc_link,
						'target' => $sc_id,
						'type' => EDGE_TYPE,
						'size' => EDGE_SIZE,
						'color' => EDGE_COLOR
					];
				}
			}
		}	
		
		unset($subcourses_assoc_by_id);
		unset($subcourses_assoc_by_layer);	*/
	}
}

//return ['nodes' => $nodes, 'edges' => $edges, 'errors' => $errors];

?>