<?php

include 'config.php';
include 'functions.php';
		
dbconnect();

$course_id = 18;
$subcourses = getCourseSubcourses($course_id); // подкурсы и связи между ними (отсортирован по количеству предков)

if($subcourses)
{
	$subcourses_assoc_by_id = []; // ключ - id подкурса, значение - массив: [массив связей К от других, массив связей ОТ к другим, название, номер слоя]
	$subcourses_assoc_by_layer = []; // ключ - номер слоя, значение - массив id`s элементов

	foreach($subcourses as &$subcourse)
	{
		$subcourse['links_to'] = getCourseLinks($subcourse['id'][0], true); // получить связи К элементам-потомкам
		
		$links = getCourseLinksFromToIds($subcourse['links'], $subcourse['links_to']); // получить связи в виде двух массивов с id`s элементов
		
		$subcourses_assoc_by_id[$subcourse['id'][0]] = ['links_from' => $links['from'], 'links_to' => $links['to'], 'name' => $subcourse['name'], 'layer' => -1, 'added' => false];
	}
	unset($subcourse);
	unset($links);
	unset($subcourses);

	$min_layer = 0; // слой, с которого начинается 3-й этап 

	// 1 // Получаем минимальные узлы (не имеющие связей от других узлов) 

	foreach($subcourses_assoc_by_id as $id => &$subcourse)
	{
		if(count($subcourse['links_from']) == 0)
		{// выбираем элементы без предков

			$subcourse['layer'] = 0; 								 // назначаем первый слой
			$subcourses_assoc_by_layer[$subcourse['layer']][] = $id; // добавляем элемент в первый слой
		}
		else { break; }	// как только элементы без предков заканчиваются, идём дальше
	}
	unset($subcourse);


	// 2 // Устанавливаем минимальный элемент решётки

	if(count($subcourses_assoc_by_layer[0]) > 1)
	{// если минимальных элементов больше 1, то добавляем для них общий минимальный элемент и назначаем каждому из них связь с добавленным элементом 
		
		// добавляем новый курс
		$new_min_subcourse_id = addCourse(NEW_MIN_SUBCOURSE_NAME);
		saveCourse($new_min_subcourse_id, NEW_MIN_SUBCOURSE_NAME.$new_min_subcourse_id);
		
		// делаем его подкурсом курса course_id
		addSubcourse($course_id, $new_min_subcourse_id);
		
		// добавляем связи от нового элемента к элементам минимального слоя
		foreach($subcourses_assoc_by_layer[0] as $min_subcourse_id)
		{
			// добавляем каждую связь в бд
			addCourseLink($new_min_subcourse_id, $min_subcourse_id);
			
			// обновляем связи
			$links = getCourseLinksFromToIds(getCourseLinks($min_subcourse_id), getCourseLinks($min_subcourse_id, true));		
			$subcourses_assoc_by_id[$min_subcourse_id]['links_from'] = $links['from'];
			$subcourses_assoc_by_id[$min_subcourse_id]['links_to'] = $links['to'];
			
			// теперь слой будет 2-ым
			$subcourses_assoc_by_id[$min_subcourse_id]['layer'] = 1;
			
			// переносим элементы из 1-го слоя во 2-й
			$subcourses_assoc_by_layer[1][] = $min_subcourse_id;		
		}
		
		// вместо старых элементов вставляем в 1-й слой один новый добавленный
		$subcourses_assoc_by_layer[0] = [ intval($new_min_subcourse_id) ];
		
		// добавляем данные о новом элементе
		$new_links = getCourseLinksFromToIds(getCourseLinks($new_min_subcourse_id), getCourseLinks($new_min_subcourse_id, true));
		$subcourses_assoc_by_id[$new_min_subcourse_id] = ['links_from' => $new_links['from'], 'links_to' => $new_links['to'], 'name' => NEW_MIN_SUBCOURSE_NAME.$new_min_subcourse_id, 'layer' => 0, 'added' => true];
		
		$min_layer = 1;
	}


	// 3 // Устанавливаем максимальный элемент решётки

	foreach($subcourses_assoc_by_id as $id => &$subcourse)
	{
		if(count($subcourse['links_to']) == 0)
		{// находим элементы без потомков

			$subcourse['layer'] = 'last'; // делаем их слой последним
			$subcourses_assoc_by_layer[$subcourse['layer']][] = $id; // добавляем элемент в последний слой
		}	
	}
	unset($subcourse);

	if(count($subcourses_assoc_by_layer['last']) > 1)
	{// если последних элементов больше 1, то добавляем для них общий максимальный элемент и назначаем ему связь с каждым из них 
		
		// добавляем новый курс
		$new_max_subcourse_id = addCourse(NEW_MAX_SUBCOURSE_NAME);
		saveCourse($new_max_subcourse_id, NEW_MAX_SUBCOURSE_NAME.$new_max_subcourse_id);
		
		// делаем его подкурсом курса course_id
		addSubcourse($course_id, $new_max_subcourse_id);
		
		// добавляем связи от элементов максимального слоя к новому элементу
		foreach($subcourses_assoc_by_layer['last'] as $max_subcourse_id)
		{
			// добавляем каждую связь в бд
			addCourseLink($max_subcourse_id, $new_max_subcourse_id);
			
			// обновляем связи
			$links = getCourseLinksFromToIds(getCourseLinks($max_subcourse_id), getCourseLinks($max_subcourse_id, true));		
			$subcourses_assoc_by_id[$max_subcourse_id]['links_from'] = $links['from'];
			$subcourses_assoc_by_id[$max_subcourse_id]['links_to'] = $links['to'];
		}
		
		// добавляем новый слой с добавленный максимальным элементом
		// $subcourses_assoc_by_layer['new_last'] = [ intval($new_max_subcourse_id) ];
		
		// добавляем данные о новом элементе
		$new_links = getCourseLinksFromToIds(getCourseLinks($new_max_subcourse_id), getCourseLinks($new_max_subcourse_id, true));
		$subcourses_assoc_by_id[$new_max_subcourse_id] = ['links_from' => $new_links['from'], 'links_to' => $new_links['to'], 'name' => NEW_MAX_SUBCOURSE_NAME.$new_max_subcourse_id, 'layer' => 'new_last', 'added' => true];
	}
	unset($subcourses_assoc_by_layer['last']);

	// 4 // Назначаем каждому элементу слой (для y-координаты)

	/// Работа начинается с минимального слоя. После расчёта фильтров всех его элементов становятся известны все слои. Для элементов этих (всех остальных) слоёв операция расчёта фильтров выполняется отдельно 
	// получаем элементы минимального слоя 
	$min_layer_subcourses = []; // массив id`s элементов слоя
	foreach($subcourses_assoc_by_layer[$min_layer] as $subcourse_id)
	{
		$min_layer_subcourses[] = $subcourse_id; // заполняем массив
	}

	// подготавливаем полученные элементы 
	$layer_subcourses_filters = []; // массив главных фильтров элементов слоя (id`s ВСЕХ потомков)
	$layer_subcourses_pairs = []; // массив пар элементов слоя (id`s)
	foreach($min_layer_subcourses as $mls)
	{
		// ищем ФИЛЬТРЫ для элементов МИНИМАЛЬНОГО СЛОЯ и обновляем НОМЕРА СЛОЁВ ВСЕХ элементов (subcourses_assoc_by_id) и ЭЛЕМЕНТОВ В этих СЛОЯХ (subcourses_assoc_by_layer)
		getSubcoursesFilters($layer_subcourses_filters[$mls], $subcourses_assoc_by_id[$mls]['links_to'], $subcourses_assoc_by_id, $subcourses_assoc_by_layer, $min_layer, $min_layer+1, true);	
	}

	// здесь необходимо убирать последний слой, но это делается далее перед шагом 5.2, поскольку здесь по какой-то причине не отрабатывает

	/// Расчёт фильтров всех остальных элементов и составление пар элементов в слое
	foreach($subcourses_assoc_by_layer as $num_layer => $layer_subcourses)
	{	
		// фильтры
		foreach($layer_subcourses as $ls)
		{
			getSubcoursesFilters($layer_subcourses_filters[$ls], $subcourses_assoc_by_id[$ls]['links_to'], $subcourses_assoc_by_id, $subcourses_assoc_by_layer, $min_layer, $num_layer+1, false);
		}
		
		// составляем всевозможные пары элементов слоя num_layer, без указаний на самих себя и повторений (т.е. не 1-2, 2-1, 1-1, 2-2, а только 1-2)
		$layer_subcourses_pairs[$num_layer] = [];
		foreach($layer_subcourses as $ls1)
		{
			foreach($layer_subcourses as $ls2)
			{
				if($ls1 != $ls2 && !in_array([$ls2, $ls1], $layer_subcourses_pairs[$num_layer]))
					$layer_subcourses_pairs[$num_layer][] = [$ls1, $ls2];
			}
		}
	}
	
	unset($min_layer);

	// 5 // Выполняем для каждой пары элементов (в одном слое) следующие действия

	$min_subcourses = []; // минимальные элементы фильтров
	$key_for_min_elements = 0; // для сохранения минимальных элементов

	// 5.1 // находим минимальные элементы каждого фильтра
	foreach($layer_subcourses_pairs as $layer => $pairs)
	{
		// перебираем пары элементов в слое
		foreach($pairs as $pair)
		{
			// находятся пересечения фильтров для каждой пары элементов
			$filter_intersection = array_intersect($layer_subcourses_filters[$pair[0]], $layer_subcourses_filters[$pair[1]]);
					
			// находим минимальные элементы пересечения фильтров
			$filter_subcourses = []; // элементы в контексте фильтра
			
			foreach($filter_intersection as $subcourse_id)
			{					
				$filter_subcourses[$subcourse_id]['elem1'] = $pair[0];
				$filter_subcourses[$subcourse_id]['elem2'] = $pair[1];
				
				$filter_subcourses[$subcourse_id]['links_from'] = $filter_subcourses[$subcourse_id]['links_to'] = [];
				
				// получить связи элемента пересечения 
				// ОТ других
				if(!is_null($subcourses_assoc_by_id[$subcourse_id]['links_from']))
				{
					foreach($subcourses_assoc_by_id[$subcourse_id]['links_from'] as $link_from)
					{
						if(in_array($link_from, $filter_intersection)) // если элемент-источник внутри пересечения фильтров
							$filter_subcourses[$subcourse_id]['links_from'][] = $link_from; // добавим связь, если она принадлежит данному фильтру
					}	
				}
				
				if(count($filter_subcourses[$subcourse_id]['links_from']) == 0) // элементы-источники для subcourse_id ВНЕ фильтра
				{
					$min_subcourses[$key_for_min_elements]['elem1'] = $filter_subcourses[$subcourse_id]['elem1']; // элементы, для которых искалось
					$min_subcourses[$key_for_min_elements]['elem2'] = $filter_subcourses[$subcourse_id]['elem2']; // пересечение фильтров
					$min_subcourses[$key_for_min_elements]['data'][] = 				
					[
						'min_id' => $subcourse_id // минимальный элемент пересечения фильтров
					];
				}
			}
			
			$key_for_min_elements++;
		}
	}

	unset($subcourses_assoc_by_layer[count($subcourses_assoc_by_layer)-1]); // убираем последний слой, поскольку функция отрабатывает в последний раз на ПОТОМКАХ МАКСИМАЛЬНОГО элемента решётки

	// 5.2 // на основе полученных минимальных элементов, если необходимо, создаём для фильтров новые минимальные элементы
	foreach($min_subcourses as $key_min => $subcourse)
	{
		if(count($subcourse['data']) > 1)
		{
			// удаляем старые связи
			$deletedLinks = []; // для восстановления связей уже с новым минимальным элементом фильтра
			foreach($subcourse['data'] as $data)
			{				
				// перебираем связи к минимальным элементам фильтра
				foreach($subcourses_assoc_by_id[$data['min_id']]['links_from'] as $lnk)
				{
					// если связь не внутри фильтра, удалить её и сохранить для связи вдальнейшем с новым элементом
					if(!in_array($lnk, array_intersect($layer_subcourses_filters[$subcourse['elem1']], $layer_subcourses_filters[$subcourse['elem2']]))
						&& ($lnk == $subcourse['elem1'] || $lnk == $subcourse['elem2']))
					{
						deleteCourseLink($lnk, $data['min_id']);
						if(!in_array($lnk, $deletedLinks))
						{
							$deletedLinks[] = $lnk;
						}
					}
				}
			}
			
			$okey = 1;
			foreach($subcourse['data'] as $data)
			{
				if(!in_array($subcourse['elem1'], $subcourses_assoc_by_id[$data['min_id']]['links_from']) || !in_array($subcourse['elem2'], $subcourses_assoc_by_id[$data['min_id']]['links_from']))
				{// между elem1 и elem2 есть другие элементы
					$okey = 0;
					break;
				}
			}
			
			if($okey)
			{
				$new_name = $subcourses_assoc_by_id[$subcourse['elem1']]['name'].' + '.$subcourses_assoc_by_id[$subcourse['elem2']]['name'];
				$new_layer = max($subcourses_assoc_by_id[$subcourse['elem1']]['layer'], $subcourses_assoc_by_id[$subcourse['elem2']]['layer']) + 1;
								
				// добавляем новый минимальный элемент фильтра
				$new_min_subcourse_id = addCourse($new_name);
				
				// делаем его подкурсом курса course_id
				addSubcourse($course_id, $new_min_subcourse_id);
						
				// добавляем (восстанавливаем старые) связи от элементов, которые были связаны с минимальными элементами фильтра, к новому минимальному элементу
				foreach($deletedLinks as $del_link)
				{
					addCourseLink($del_link, $new_min_subcourse_id);
				}
				
				// обновляем ссылки элементов, для которых искали пересечение фильтра
				$elem1_links = getCourseLinksFromToIds(getCourseLinks($subcourse['elem1']), getCourseLinks($subcourse['elem1'], true));
				$subcourses_assoc_by_id[$subcourse['elem1']]['links_from'] = $elem1_links['from'];
				$subcourses_assoc_by_id[$subcourse['elem1']]['links_to'] = $elem1_links['to'];
				
				$elem2_links = getCourseLinksFromToIds(getCourseLinks($subcourse['elem2']), getCourseLinks($subcourse['elem2'], true));
				$subcourses_assoc_by_id[$subcourse['elem2']]['links_from'] = $elem2_links['from'];
				$subcourses_assoc_by_id[$subcourse['elem2']]['links_to'] = $elem2_links['to'];
				
				// добавляем связи от нового к бывшим минимальным элементам
				foreach($subcourse['data'] as $data)
				{
					addCourseLink($new_min_subcourse_id, $data['min_id']);
					
					$min_links = getCourseLinksFromToIds(getCourseLinks($data['min_id']), getCourseLinks($data['min_id'], true));
					$subcourses_assoc_by_id[$data['min_id']]['links_from'] = $min_links['from'];
					$subcourses_assoc_by_id[$data['min_id']]['links_to'] = $min_links['to'];	
				}
				
				// обновляем слои элементов в словаре элементов
				foreach($subcourses_assoc_by_id as &$value)	
				{
					if($value['layer'] >= $new_layer)
					{
						$value['layer'] += 1;
					}
				}
				unset($value);
				
				// добавляем новый элемент в словарь слоёв
				foreach($subcourses_assoc_by_layer as $key => $value)	
				{
					if($key == $new_layer)
					{
						array_splice($subcourses_assoc_by_layer, $key, 0, array(array(intval($new_min_subcourse_id))));
						break;
					}
				}
				unset($value);
					
				// добавляем новый элемент в словарь элементов
				$new_links = getCourseLinksFromToIds(getCourseLinks($new_min_subcourse_id), getCourseLinks($new_min_subcourse_id, true));
				$subcourses_assoc_by_id[$new_min_subcourse_id] = ['links_from' => $new_links['from'], 'links_to' => $new_links['to'], 'name' => $new_name, 'layer' => $new_layer, 'added' => true];
			}
		}
	}

	// 6 // Назначаем каждому элементу позицию в слое (для x-координаты)
	foreach($subcourses_assoc_by_layer as $sc_layer)
	{
		$count_in_layer = count($sc_layer);
		$coord = (-1) * X_STEP * $count_in_layer / 2;
					
		foreach($sc_layer as $sc_id)
		{
			$subcourses_assoc_by_id[$sc_id]['x'] = $coord;
			$coord += X_STEP;
		}
	}

	// 7 // Подготавливаем к рисованию 
	
	$nodes = [];
	$edges = [];		
	
	foreach($subcourses_assoc_by_id as $sc_id => $sc_value)
	{
		if($sc_value['added'])
			$color = NODE_COLOR_ADDED;
		else
			if(courseHasSubcourses($sc_id))
				$color = NODE_COLOR_NOTERMINAL;
			else
				$color = NODE_COLOR_TERMINAL;
			
		if($subcourses_assoc_by_id[$sc_id]['layer'] == 0)
			$url = NODE_IMAGE_MIN;
		elseif($subcourses_assoc_by_id[$sc_id]['layer'] == count($subcourses_assoc_by_layer)-1)
			$url = NODE_IMAGE_MAX;
		else
			if(courseHasContent($sc_id))
				$url = NODE_IMAGE_CONTENT;
			else
				$url = '';
		
		$x = $sc_value['x'];
		$y = (-1) * Y_STEP * $sc_value['layer'];
		
		$nodes[] = [
			'id' => $sc_id, // id элемента как курса
			'label' => $sc_value['name'], // название
			'x' => $x, 
			'y' => $y, 
			'size' => NODE_SIZE,
			'color' => $color,
			'type' => 'circle',
			'image' => ['url' => $url, 'scale '=> 1, 'clip' => 1]
		];
		
		if(!is_null($sc_value['links_from']))
		{
			// добавляем все связи, относящиеся к узлу (подкурсу)
			foreach($sc_value['links_from'] as $sc_link)
			{
				$edges[] = [
					'id' => $sc_link.'-'.$sc_id,
					'source' => $sc_link,
					'target' => $sc_id,
					'type' => EDGE_TYPE,
					'size' => EDGE_SIZE,
					'color' => EDGE_COLOR
				];
			}
		}
	}
}

dbclose();

/*echo '<pre>';
var_dump($subcourses_assoc_by_layer);
echo '</pre>';echo '</br>';*/

?>