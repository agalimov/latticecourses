<?php

    $db_con = null;

    /* соединение с бд */
    function dbconnect()
    {
        ini_set('default_charset', 'UTF-8');
        global $DB_DATABASE, $DB_HOST, $DB_USER, $DB_PASSWORD, $db_con;
        $db_con = new PDO('mysql:dbname='.$DB_DATABASE.';host='.$DB_HOST.';charset=utf8', $DB_USER, $DB_PASSWORD);
        $db_con->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $db_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $db_con;
    }

    /* закрытие соединения */
    function dbclose()
	{
        global $db_con;
        $db_con = null;
    }	
	
    /* получить таблицу с префиксом */
	function table($table)
	{
        global $DB_PREFIX;
		return $DB_PREFIX.$table;
	}
	
	/* получить контент курса */
	function getCourseContent($id)
	{
        global $db_con;
		$query = 'SELECT * FROM '.table('courses_contents').' WHERE course_id = ?';
		$sql = $db_con->prepare($query);
		$sql->execute(array($id));
		$contents = $sql->fetchAll(PDO::FETCH_NAMED);
		return $contents;
	}
	
	/* получить связи курса */
    function getCourseLinks($id)
	{
        global $db_con;
		$query = 'SELECT * FROM '.table('courses_links').' INNER JOIN '.table('courses').' ON '.table('courses_links').'.src_id = '.table('courses').'.id AND '.table('courses_links').'.dest_id = ?';
		$sql = $db_con->prepare($query);
		$sql->execute(array($id));
		$links = $sql->fetchAll(PDO::FETCH_NAMED);
		return $links;
	}
		
	/* получить подкурсы курса */
	function getCourseSubcourses($id)
	{
        global $db_con;
		$query = 'SELECT * FROM '.table('subcourses').' INNER JOIN '.table('courses').' ON '.table('subcourses').'.subcourse_id = '.table('courses').'.id AND '.table('subcourses').'.course_id = ?';
		$sql = $db_con->prepare($query);
		$sql->execute(array($id));
		$subcourses = $sql->fetchAll(PDO::FETCH_NAMED);
		
		foreach($subcourses as &$sc)
		{
			$sc['links'] = getCourseLinks($sc['subcourse_id']);
		}
		
		return $subcourses;
	}	
	
	/* получить связи подкурсов заданного курса */
	/*function getSubcoursesLinks($subcourses)
	{
		foreach($subcourses as $sc)
		{
			$subcourses_links[$sc['id']] = getCourseLinks($sc['id']);
		}
		return $subcourses_links;
	}	*/
	
    /* получить курс по id */
    function getCourse($id)
	{
        global $db_con;
        $query = 'SELECT * FROM '.table('courses').' WHERE id = ?';
        $sql = $db_con->prepare($query);
        $sql->execute(array($id));
        $res = $sql->fetch(PDO::FETCH_ASSOC);
		
		if($res)
		{
			$res['contents'] = getCourseContent($id); // Контент			
			$res['links'] = getCourseLinks($id); // Связи					
			$res['subcourses'] = getCourseSubcourses($id); // Подкурсы и связи между ними			
		}
		else
		{
			$res['id'] = 0;
		}
		
		return $res;
    }
	
	
	
	
	
	


    /*получить пользователя по email*/
    function getUser($email)
	{
        global $db_con;
        $matchrow = null;
        $query = "select id, family_id, name, password from user where email = ?";
        $sql = $db_con->prepare($query);
        $sql->execute(array($email));
        $rows = $sql->fetchAll(PDO::FETCH_ASSOC);
        foreach($rows as $row){
            $matchrow = $row;
        }
        return $matchrow;
    }

    /* получить пользователя по id*/
    function getUserById($id){
        global $db_con;
        $matchrow = null;
        $query = "select * from user where id = ?";
        $sql = $db_con->prepare($query);
        $sql->execute(array($id));
        $rows = $sql->fetchAll(PDO::FETCH_ASSOC);
        foreach($rows as $row){
            $matchrow = $row;
        }
        return $matchrow;
    }

    /* получить семью по названию */
    function getFamilyByName($name) {
        global $db_con;
        $matchrow = null;
        $query = "select * from family where family_name = ?";
        $sql = $db_con->prepare($query);
        $sql->execute(array($name));
        $rows = $sql->fetchAll(PDO::FETCH_ASSOC);
        foreach ($rows as $row) {
            $matchrow = $row;
        }
        return $matchrow;
    }

    /*получить пользователя по email*/
    function getUserByEmail($email) {
        global $db_con;
        $matchrow = null;
        $query = "select * from user where email = ?";
        $sql = $db_con->prepare($query);
        $sql->execute(array($email));
        $rows = $sql->fetchAll(PDO::FETCH_ASSOC);
        foreach ($rows as $row) {
            $matchrow = $row;
        }
        return $matchrow;
    }

    /* удалить члена семьи */
    function deleteMember($id, $family_id) {
        global $db_con;
        $sql = $db_con->prepare("delete from user where id = ? && family_id=?");;
        $sql->execute(array($id, $family_id));
    }

    /* создать семью */
    function insertFamily($name) {
        global $db_con;
        $sql = $db_con->prepare("INSERT INTO family(family_name) VALUES(?)");
        $sql->execute(array($name));
        $id = $db_con->lastInsertId();
        return $id;
    }

    /* изменить семью */
    function updateFamily($id, $family_name){
        global $db_con;
        $query = "update family set family_name = ? where id = ?;";
        $sql = $db_con->prepare($query);
        $sql->execute(array($family_name, $id));
    }

    /* получить семью по пользователю */
    function getFamilyByUserId($id) {
        global $db_con;
        $matchrow = null;
        $query = "select * from user where id = ?";
        $sql = $db_con->prepare($query);
        $sql->execute(array($id));
        $rows = $sql->fetchAll(PDO::FETCH_ASSOC);
        $family_id = null;
        foreach ($rows as $row) {
            $family_id = $row['family_id'];
        }
        $query = "select * from family where id = ?";
        $sql = $db_con->prepare($query);
        $sql->execute(array($family_id));
        $rows = $sql->fetchAll(PDO::FETCH_ASSOC);
        foreach ($rows as $row) {
            $matchrow= $row;
        }
        return $matchrow['family_name'];
    }

    /* получить членов семьи по id */
    function getFamilyMembers($family_id)
    {
        global $db_con;
        $matchrow = null;
        $query = "select * from user where family_id = ?";
        $sql = $db_con->prepare($query);
        $sql->execute(array($family_id));
        $rows = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    /* сохранить пользователя */
    function storeUser($username, $email, $password){
        global $db_con;
        $sql = $db_con->prepare("INSERT INTO user(name, email, password, created_at) VALUES(?, ?, ?, NOW())");
        $sql->execute(array($username, $email, $password));
        $id = $db_con->lastInsertId();
        return $id;
    }

    /* обновить пользователя */
    function updateUserWithFamilyName($id, $family_id) {
        global $db_con;
        $sql = $db_con->prepare("update user set family_id = ? where id = ?");
        $sql->execute(array($family_id, $id));
    }

    /* добавить семью */
    function addFamily($family_name){
        global $db_con;
        $sql = $db_con->prepare("INSERT INTO family(family_name) VALUES(?)");
        $sql->execute(array($family_name));
        $id = $db_con->lastInsertId();
        return $id;
    }

    /* добавить члена семьи */
    function addMember($username, $email, $password, $family_id)
    {
        global $db_con;
        $sql = $db_con->prepare("INSERT INTO user(family_id, name, email, password, created_at) VALUES(?, ?, ?, ?, NOW())");
        $sql->execute(array($family_id, $username, $email, $password));
        $id = $db_con->lastInsertId();
        return $id;
    }

    /* данные о задаче */
    class taskStruct
    {
        public $taskData;
        public $assignedToName;
    }

    /* получить задачи семьи */
    function getFamilyTasks($members)
    {
        global $db_con;

        $tsres = array();

        foreach($members as $row)
        {
            $sql = $db_con->prepare("select todo.id as tid,todo.name as tname,todo.assigned_to_id as tassign,todo.creator_id as tcreat,todo.completed as tcomp from todo where todo.creator_id = ?");
            $sql->execute(array($row['id']));
            $res = $sql->fetchAll(PDO::FETCH_ASSOC);

            foreach ($res as $r)
            {
                $ts = new taskStruct();
                $ts->taskData = $r;

                if($ts->taskData['tassign'] == null)
                {
                    $ts->assignedToName['name'] = 'all';
                }
                else
                {
                    $sql = $db_con->prepare("select name from user where user.id = ?");
                    $sql->execute(array($ts->taskData['tassign']));
                    $res_ = $sql->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($res_ as $r) { $ts->assignedToName = $r; }
                }

                $tsres[] = $ts;
            }
        }

        return $tsres;
    }

    /* получить общие задачи семьи */
    function getFamilyCommonTasks($members)
    {
        global $db_con;

        $tsres = array();

        foreach($members as $row)
        {
            $sql = $db_con->prepare("select todo.id as tid,todo.name as tname,todo.assigned_to_id as tassign,todo.creator_id as tcreat,todo.completed as tcomp from todo where todo.creator_id = ?");
            $sql->execute(array($row['id']));
            $res = $sql->fetchAll(PDO::FETCH_ASSOC);

            foreach ($res as $r)
            {
                $ts = new taskStruct();
                $ts->taskData = $r;

                if($ts->taskData['tassign'] == null)
                {
                    $ts->assignedToName['name'] = 'all';
                    $tsres[] = $ts;
                }
                /*else
                {
                    $sql = $db_con->prepare("select name from user where user.id = ?");
                    $sql->execute(array($ts->taskData['tassign']));
                    $res_ = $sql->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($res_ as $r) { $ts->assignedToName = $r; }
                }

                $tsres[] = $ts;*/
            }
        }

        return $tsres;
    }

    /* получить задачи пользователя */
    function getUserTasks($uId)
    {
        global $db_con;

        $tsres = array();

        $sql = $db_con->prepare("select todo.id as tid,todo.name as tname,todo.assigned_to_id as tassign,todo.creator_id as tcreat,todo.completed as tcomp from todo where todo.assigned_to_id = ?");
        $sql->execute(array($uId));
        $res = $sql->fetchAll(PDO::FETCH_ASSOC);

        foreach ($res as $r)
        {
            $ts = new taskStruct();
            $ts->taskData = $r;
            $u = getUserById($uId);
            $ts->assignedToName['name'] = $u['name'];

            $tsres[] = $ts;
        }

        return $tsres;
    }

    /* получить информацию о задаче */
    function getTaskInfoDb($par)
    {
        global $db_con;

        $res = new taskStruct();

        $sql = $db_con->prepare("select * from todo where todo.id = ?");
        $sql->execute(array($par));
        $res_ = $sql->fetchAll(PDO::FETCH_ASSOC);
        foreach ($res_ as $r) { $res->taskData = $r; }

        if($res->taskData['assigned_to_id'] == null)
        {
            $res->assignedToName['name'] = 'all';
        }
        else
        {
            $sql = $db_con->prepare("select name from user where user.id = ?");
            $sql->execute(array($res->taskData['assigned_to_id']));
            $res_ = $sql->fetchAll(PDO::FETCH_ASSOC);
            foreach ($res_ as $r) { $res->assignedToName = $r; }
        }

        return $res;
    }

    /* обновить информацию о задаче*/
    function updateTask($task_id,$task_name,$task_type,$task_assignedto,$task_content,
                        $task_timedep,$task_datetime)
    {
        global $db_con;
        $query = "update todo set assigned_to_id = ?, name = ?, type = ?, content = ?,
                                        time_independent = ?, date_time = ? where id = ?";
        $sql = $db_con->prepare($query);
        $sql->execute(array($task_assignedto,$task_name,$task_type,$task_content,$task_timedep,
            $task_datetime,$task_id));
    }

    /* удалить задачу */
    function deleteTask($task_id)
    {
        global $db_con;
        $sql = $db_con->prepare("delete from todo where id = ?");
        $sql->execute(array($task_id));
    }

    /* получить комментарии к задаче */
    function getTaskMessages($task_id)
    {
        global $db_con;
        $sql = $db_con->prepare("select * from message where todo_id = ? order by date_time asc");
        $sql->execute(array($task_id));
        $res = $sql->fetchAll(PDO::FETCH_ASSOC);

        return $res;
    }

    /* добавить комментарий к задаче */
    function addTaskComment($todo_id, $user_id, $mess_content)
    {
        global $db_con;
        $sql = $db_con->prepare("INSERT INTO message(todo_id, user_id, content, date_time) VALUES(?, ?, ?, NOW())");
        $sql->execute(array($todo_id, $user_id, $mess_content));
        $db_con->lastInsertId();
    }

    /* добавить новую задачу */
    function addNewTask($task_name,$task_type,$task_creator,$task_assignedto,$task_content,
                        $task_timedep,$task_datetime,$task_completed)
    {
        global $db_con;
        $sql = $db_con->prepare("INSERT INTO todo(creator_id, assigned_to_id, name, type, content, date_time, time_independent, completed) VALUES(?, ?, ?, ?, ?, ?, ?, ?)");
        $sql->execute(array($task_creator,$task_assignedto,$task_name,$task_type,$task_content,$task_datetime,$task_timedep,$task_completed));
        $id = $db_con->lastInsertId();
        return $id;
    }

    /* редактировать выполненность задачи */
    function editComplete($task_id)
    {
        global $db_con;
        $comp=0;

        $sql = $db_con->prepare("select completed from todo where id = ?");
        $sql->execute(array($task_id));
        $res = $sql->fetchAll(PDO::FETCH_ASSOC);
        foreach ($res as $row)
        {
            $comp = $row['completed'];
        }

        if($comp == 0)
            $comp = 1;
        else
            $comp = 0;

        $sql = $db_con->prepare("update todo set completed = ? where id = ?");
        $sql->execute(array($comp, $task_id));
    }
?>