-- phpMyAdmin SQL Dump
-- version 4.0.10.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 14 2016 г., 00:44
-- Версия сервера: 5.5.45
-- Версия PHP: 5.4.44

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `lattice_courses`
--

-- --------------------------------------------------------

--
-- Структура таблицы `ll_courses`
--

CREATE TABLE IF NOT EXISTS `ll_courses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `active` int(2) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Дамп данных таблицы `ll_courses`
--

INSERT INTO `ll_courses` (`id`, `name`, `active`) VALUES
(1, 'Первый курс', 1),
(2, 'Второй курс', 1),
(3, 'Третий курс', 1),
(4, 'Четвертый курс', 1),
(5, 'Пятый курс', 1),
(6, 'Шестой курс', 1),
(7, 'Седьмой курс', 1),
(8, 'Восьмой курс', 1),
(9, 'Девятый курс', 1),
(10, 'Десятый курс', 1),
(11, 'Одиннадцатый курс', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `ll_courses_contents`
--

CREATE TABLE IF NOT EXISTS `ll_courses_contents` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(11) unsigned NOT NULL,
  `file_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `file_id` (`file_id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `ll_courses_contents`
--

INSERT INTO `ll_courses_contents` (`id`, `course_id`, `file_id`) VALUES
(1, 1, 1),
(2, 6, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `ll_courses_links`
--

CREATE TABLE IF NOT EXISTS `ll_courses_links` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `src_id` int(11) unsigned NOT NULL,
  `dest_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `src_id` (`src_id`),
  KEY `dest_id` (`dest_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Дамп данных таблицы `ll_courses_links`
--

INSERT INTO `ll_courses_links` (`id`, `src_id`, `dest_id`) VALUES
(8, 2, 3),
(9, 3, 4),
(10, 3, 5),
(11, 5, 6),
(12, 5, 7),
(13, 5, 8),
(14, 6, 9),
(15, 7, 9),
(16, 8, 9),
(17, 9, 11),
(18, 10, 11),
(20, 2, 10);

-- --------------------------------------------------------

--
-- Структура таблицы `ll_files`
--

CREATE TABLE IF NOT EXISTS `ll_files` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `source` varchar(512) NOT NULL,
  `format_id` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `format_id` (`format_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `ll_files`
--

INSERT INTO `ll_files` (`id`, `source`, `format_id`) VALUES
(1, 'qwer', 3),
(2, 'tyui', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `ll_formats`
--

CREATE TABLE IF NOT EXISTS `ll_formats` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `image` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `ll_formats`
--

INSERT INTO `ll_formats` (`id`, `name`, `image`) VALUES
(1, 'png', 'format_png.png'),
(2, 'jpg', 'format_jpg.png'),
(3, 'doc', 'format_doc.png'),
(4, 'ppt', 'format_ppt.png'),
(5, 'pdf', 'format_pdf.png');

-- --------------------------------------------------------

--
-- Структура таблицы `ll_subcourses`
--

CREATE TABLE IF NOT EXISTS `ll_subcourses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(11) unsigned NOT NULL,
  `subcourse_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `course_id` (`course_id`),
  KEY `subcourse_id` (`subcourse_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Дамп данных таблицы `ll_subcourses`
--

INSERT INTO `ll_subcourses` (`id`, `course_id`, `subcourse_id`) VALUES
(3, 1, 2),
(5, 1, 3),
(6, 1, 4),
(10, 1, 5),
(11, 1, 6),
(12, 1, 7),
(13, 1, 8),
(14, 1, 9),
(15, 1, 10),
(16, 1, 11),
(20, 9, 6);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `ll_courses_contents`
--
ALTER TABLE `ll_courses_contents`
  ADD CONSTRAINT `ll_courses_contents_ibfk_2` FOREIGN KEY (`file_id`) REFERENCES `ll_files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ll_courses_contents_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `ll_courses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `ll_courses_links`
--
ALTER TABLE `ll_courses_links`
  ADD CONSTRAINT `ll_courses_links_ibfk_1` FOREIGN KEY (`src_id`) REFERENCES `ll_courses` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `ll_courses_links_ibfk_2` FOREIGN KEY (`dest_id`) REFERENCES `ll_courses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `ll_files`
--
ALTER TABLE `ll_files`
  ADD CONSTRAINT `ll_files_ibfk_1` FOREIGN KEY (`format_id`) REFERENCES `ll_formats` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `ll_subcourses`
--
ALTER TABLE `ll_subcourses`
  ADD CONSTRAINT `ll_subcourses_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `ll_courses` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `ll_subcourses_ibfk_2` FOREIGN KEY (`subcourse_id`) REFERENCES `ll_courses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
